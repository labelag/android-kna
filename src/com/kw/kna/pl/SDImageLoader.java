package com.kw.kna.pl;

import java.lang.ref.WeakReference;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.kw.kna.R;
import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;
import com.kw.kna.io.FileServices;

public class SDImageLoader {

	private int mListThumbWidth = 150;
	private ScaleType scaleTypeBackup;
	private boolean roundedCorner;

	public void load(String filePath, ImageView v) {
		load(filePath, v, true);
	}

	public void load(String filePath, ImageView v, boolean forListView) {
		if (filePath == null)
			return;
		if (cancelPotentialSDLoad(filePath, v)) {
			SDLoadImageTask task = new SDLoadImageTask(v, forListView);
			v.setTag(R.id.tag_SDLoadTask, new WeakReference<SDLoadImageTask>(
					task));
			scaleTypeBackup = v.getScaleType();
			if (FileServices.checkFileExist(filePath)) {
				// SDLoadDrawable sdDrawable = new SDLoadDrawable(task);
				v.setImageDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
				task.execute(filePath);
			} else {
				// GeneralPLS.showToast("dl image : " + filePath);
				v.setImageResource(R.drawable.loading_indicator);
				v.setScaleType(ScaleType.CENTER);
				v.startAnimation(PLConfig.anim_syncer_rotate());
				task.setToBeDownload(true);
				task.executeOnParallel(filePath);
			}
		}
	}

	public void setListThumbWidth(int mListThumbWidth) {
		this.mListThumbWidth = mListThumbWidth;
	}

	public void setRoundedCorner(boolean roundedCorner) {
		this.roundedCorner = roundedCorner;
	}

	private Bitmap loadImageFromSDCard(String filePath, boolean forList) {
		Bitmap photo = null;
		try {
			if (!FileServices.checkFileExist(filePath)) {
				return BitmapFactory.decodeResource(
						AppConfig.Application.getResources(),
						android.R.drawable.ic_menu_report_image);
			}
			BitmapFactory.Options bfo = new BitmapFactory.Options();

			bfo.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(filePath, bfo);
			if (forList) {
				bfo.inSampleSize = calculateInSampleSize(bfo, mListThumbWidth,
						mListThumbWidth);// bfo.outWidth / mListThumbWidth;
				bfo.outWidth = mListThumbWidth;
				bfo.outHeight = mListThumbWidth;
			} else
				bfo.inSampleSize = calculateInSampleSize(bfo,
						PLConfig.ScreenWidth, PLConfig.ScreenHeight);
			bfo.inJustDecodeBounds = false;
			photo = BitmapFactory.decodeFile(filePath, bfo);
			if (roundedCorner)
				photo = getRoundedCornerBitmap(photo,
						PLConfig.List_Thumb_Radiuspx);
		} catch (Exception e) {
			Logger.DoLog("load image form SD card failed!", e.getMessage(),
					LogType.Error);
		}
		return photo;

	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					|| (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
		try {
			Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
					bitmap.getHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(output);

			final int color = 0xff424242;
			final Paint paint = new Paint();
			final Rect rect = new Rect(0, 0, bitmap.getWidth(),
					bitmap.getHeight());
			final RectF rectF = new RectF(rect);
			final float roundPx = pixels;

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			canvas.drawBitmap(bitmap, rect, rect, paint);

			return output;

		} catch (Exception e) {
			Logger.DoLog("get rounded corner bitmap failed!", e.getMessage(),
					LogType.Error);
			return bitmap;
		}
	}

	private static boolean cancelPotentialSDLoad(String filePath, ImageView v) {
		SDLoadImageTask sdLoadTask = getAsyncSDLoadImageTask(v);
		if (sdLoadTask != null) {
			String path = sdLoadTask.getFilePath();
			if ((path == null) || (!path.equals(filePath))) {
				sdLoadTask.cancel(true);
			} else {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private static SDLoadImageTask getAsyncSDLoadImageTask(ImageView v) {
		if (v != null) {
			Object tag = v.getTag(R.id.tag_SDLoadTask);
			// Drawable drawable = v.getDrawable();
			if (tag != null && tag instanceof WeakReference<?>) {
				// SDLoadDrawable asyncLoadedDrawable = (SDLoadDrawable)
				// drawable;
				// return asyncLoadedDrawable.getAsyncSDLoadTask();
				return ((WeakReference<SDLoadImageTask>) tag).get();
			}
		}
		return null;
	}

	private class SDLoadImageTask extends AsyncTask<String, Void, Bitmap> {

		private String mFilePath;
		private final WeakReference<ImageView> mImageViewReference;
		private boolean mForListView;
		private boolean mToBeDownload;
		private String mImageUrl;

		public String getFilePath() {
			return mFilePath;
		}

		public void setToBeDownload(boolean b) {
			this.mToBeDownload = b;

		}

		public SDLoadImageTask(ImageView v, boolean forList) {
			mImageViewReference = new WeakReference<ImageView>(v);
			mImageUrl = (String) v.getTag();
			mForListView = forList;
		}

		@Override
		protected void onPostExecute(Bitmap bmp) {
			if (mImageViewReference != null && bmp != null) {
				ImageView v = mImageViewReference.get();
				SDLoadImageTask sdLoadTask = getAsyncSDLoadImageTask(v);
				// Change bitmap only if this process is still associated with
				// it
				if (this == sdLoadTask && v != null) {
					v.clearAnimation();
					v.setImageBitmap(bmp);

					v.setScaleType(scaleTypeBackup);
				}
				bmp = null;
			}
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			mFilePath = params[0];
			if (mToBeDownload && mImageUrl != null) {
				// boolean updateRes =
				new FileServices().updateDocImage(mFilePath, mImageUrl);
				// if (!updateRes) {
				// return BitmapFactory.decodeResource(
				// AppConfig.Application.getResources(),
				// android.R.drawable.ic_menu_report_image);
				// }
			}
			return loadImageFromSDCard(mFilePath, mForListView);
		}

		public void executeOnParallel(String... params) {

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				try {
					this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
							params);
				} catch (Exception e) {
					Logger.DoLog("execute on parallel failed!", this.getClass()
							.getName(), LogType.Error);
					// if (this.getStatus() != Status.RUNNING)
					// this.execute(params);
				}
			} else {
				this.execute(params);
			}

		}
	}

}