package com.kw.kna.pl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.Toast;

import com.kw.kna.R;
import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;
import com.kw.kna.bus.enums.AsyncOperationResult;

public class GeneralPLS {
	private static final String RSSNewsPubDateFormat = "yyyy-MM-dd";

	public static Drawable getDrawableFromData(String name) {
		try {
			return Drawable.createFromPath(AppConfig.DataFullPath + name);
		} catch (Exception e) {
			Logger.DoLog(e.getMessage(), LogType.Error);
			return null;
		}
	}

	public static void handleGeneralAsyncOperationResult(
			AsyncOperationResult res, Context cnt) {
		handleGeneralAsyncOperationResult(res, cnt, true);
	}

	public static void handleGeneralAsyncOperationResult(
			AsyncOperationResult res, Context cnt, boolean handleSuccess) {
		switch (res) {
		case Success:
			if (handleSuccess)
				Toast.makeText(cnt, R.string.operation_success,
						Toast.LENGTH_LONG).show();
			break;
		case ConnectionError:
			Toast.makeText(cnt, R.string.m_async_op_connection_error,
					Toast.LENGTH_LONG).show();
			break;
		case NetworkNotAvailable:
			Toast.makeText(cnt, R.string.m_async_op_network_error,
					Toast.LENGTH_LONG).show();
			break;
		case Failure:
			Toast.makeText(cnt, R.string.operation_failed, Toast.LENGTH_LONG)
					.show();
			break;
		default:
			break;
		}
	}

	public static void showFailureToast(Context cnt) {
		Toast.makeText(cnt, R.string.operation_failed, Toast.LENGTH_SHORT)
				.show();

	}

	public static void showToast(int messageRes) {
		showToast(AppConfig.Application.getString(messageRes));
	}

	public static void showToast(String message) {
		Toast.makeText(AppConfig.Application, message, Toast.LENGTH_SHORT)
				.show();

	}

	public static String PrepareNewsDateForList(Date dt) {
		if (dt == null)
			return null;
		return new SimpleDateFormat("E, dd MMM, yyyy", Locale.ENGLISH)
				.format(dt);
	}

	public static String PrepareNewsDateForList(String dt) {
		try {
			if (dt == null)
				return null;
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					RSSNewsPubDateFormat, Locale.ENGLISH);
			return new SimpleDateFormat("E, dd MMM, yyyy", Locale.ENGLISH)
					.format(dateFormat.parse(dt));
		} catch (ParseException e) {
			Logger.DoLog("parse rss date failed!", e.getMessage(),
					LogType.Error);
			return dt;
		}
	}

}
