package com.kw.kna.pl;

import android.util.SparseArray;

public class ListItemInfo {
	private String mText;
	private int mId;
	private String mImage;
	private boolean mShowIndicator;
	private String mDescription;
	private int mParentId;
	private Object mTag;
	private SparseArray<Object> Extras;
	private boolean mEnabled = true;

	public ListItemInfo(int id, String text, String desc, String imageName,
			boolean showIndicator) {
		mId = id;
		mText = text;
		mImage = imageName;
		mShowIndicator = showIndicator;
		mDescription = desc;
		Extras = new SparseArray<Object>();
	}

	public ListItemInfo(int id, String text, String desc, String imageName,
			boolean showIndicator, int parentId) {
		mId = id;
		mText = text;
		mImage = imageName;
		mShowIndicator = showIndicator;
		mDescription = desc;
		mParentId = parentId;
		Extras = new SparseArray<Object>();
	}

	public String getText() {
		return mText;
	}

	public int getId() {
		return mId;
	}

	public String getImage() {
		return mImage;
	}

	public boolean getShowIndicator() {
		return mShowIndicator;
	}

	public String getDescription() {
		return mDescription;
	}

	public int getParentId() {
		return mParentId;
	}

	public Object getTag() {
		return mTag;
	}

	public void setTag(Object mTag) {
		this.mTag = mTag;
	}

	public void setmText(String mText) {
		this.mText = mText;
	}

	public void addExtra(int key, Object value) {
		Extras.put(key, value);
	}

	public SparseArray<Object> getExtras() {
		return Extras;
	}

	public void setEnabled(boolean mEnabled) {
		this.mEnabled = mEnabled;
	}

	public boolean isEnabled() {
		return mEnabled;
	}

	public void setImage(String mImage) {
		this.mImage = mImage;
	}
}
