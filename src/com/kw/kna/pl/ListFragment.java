package com.kw.kna.pl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.kw.kna.R;

public class ListFragment extends Fragment implements OnClickListener,
		LoaderManager.LoaderCallbacks<List<ListItemInfo>>, OnItemClickListener {

	protected int mParentId;
	protected List<ListItemInfo> mlistItems;
	protected TwoLineListAdapter mListAdapter;
	protected int mFileId;
	private String mQuery;
	protected ArrayList<Integer> catIds;
	protected AbsListView mListView;
	protected View emptyView;
	protected View prgListLoader;
	private Class<?> mClassForClick;
	private int mOriginalFileId;
	protected ListConfig mListConfig;
	private Bundle mDestActivityParams;
	protected int mLoaderId;
	protected int mItemViewResId;
	private String mFileName;
	protected GenericListLoader mListLoader;

	public ListFragment() {
		mLoaderId = new Random().nextInt();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(mLoaderId, null, this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mListConfig = (ListConfig) getArguments().getParcelable(
				IntentExtraLexicon.ListConfig);
		if (mListConfig == null) {
			Toast.makeText(getActivity(), R.string.operation_failed,
					Toast.LENGTH_SHORT).show();
			this.getActivity().finish();
			return;
		}

		mFileId = mOriginalFileId = mListConfig.getFileId();
		mParentId = mListConfig.getParentId();
		mClassForClick = mListConfig.getClickDestActivity();
		mQuery = mListConfig.getQuery();
		if (mListConfig.isInCategoryMode()) {
			if (savedInstanceState != null) {
				mParentId = savedInstanceState.getInt(IntentExtraLexicon.Id,
						mParentId);
				catIds = savedInstanceState
						.getIntegerArrayList(IntentExtraLexicon.CatIds);
			} else {
				catIds = new ArrayList<Integer>();
			}
		}
		if (mListConfig.getItemViewResId() > 0)
			mItemViewResId = mListConfig.getItemViewResId();
		else
			mItemViewResId = R.layout.list_item_twoline_light;
		// else
		// mItemViewResId = R.layout.general_list_item;
		mListAdapter = new TwoLineListAdapter(getActivity());
		mListAdapter.setTextViewResourceId(mItemViewResId);
		mListAdapter.setThumbnailVisible(mListConfig.isThumbVisible());

		switch (mFileId) {
		case R.string.fj_member_items:
		case R.string.fj_news:
			mListAdapter.setRoundedCornerThumbs(true);
			break;
		default:
			break;
		}
	}

	public android.view.View onCreateView(android.view.LayoutInflater inflater,
			android.view.ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.general_list, null);
		mListView = (AbsListView) v.findViewById(android.R.id.list);
		mListView.setOnItemClickListener(this);
		mListView.setAdapter(mListAdapter);
		emptyView = v.findViewById(android.R.id.empty);
		prgListLoader = v.findViewById(android.R.id.progress);
		return v;

	};

	@Override
	public void onSaveInstanceState(Bundle outState) {
		if (mListConfig.isInCategoryMode()) {
			outState.putInt(IntentExtraLexicon.Id, mParentId);
			outState.putIntegerArrayList(IntentExtraLexicon.CatIds, catIds);
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onDestroy() {
		getLoaderManager().destroyLoader(mLoaderId);
		super.onDestroy();
	}

	protected void handleItemClick(int index) {
		ListItemInfo item = mlistItems.get(index);
		if (mListConfig.getOnClicklistener() != null) {
			mListConfig.getOnClicklistener().onListItemClick(item, index);
			// return;
		}

		// if we are in category mode and last child not shown , continue to
		// tree.
		if (mListConfig.isInCategoryMode()) {

			catIds.add(mParentId);
			mParentId = item.getId();
			mFileId = mListConfig.getLastLevelFileId();
			getLoaderManager().restartLoader(mLoaderId, null, this);
		} else {
			Intent i = new Intent(getActivity(), mClassForClick);
			i.putExtra(IntentExtraLexicon.Id, item.getId());
			if (mDestActivityParams != null)
				i.putExtras(mDestActivityParams);
			startActivity(i);
		}
	}

	@Override
	public void onClick(View v) {
		int index = (Integer) v.getTag();
		handleItemClick(index);
	}

	public boolean handleBack() {
		if (mListConfig.isInCategoryMode() && catIds.size() > 0) {
			mParentId = catIds.remove(catIds.size() - 1);
			mFileId = mOriginalFileId;
			getLoaderManager().restartLoader(mLoaderId, null, this);
			return true;
		} else {
			return false;
		}
	}

	public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
		handleItemClick(position);
	}

	@Override
	public Loader<List<ListItemInfo>> onCreateLoader(int arg0, Bundle arg1) {
		setLoadingShown(true);
		GenericListLoader l = new GenericListLoader(getActivity(), mFileId,
				mQuery);
		l.setFileName(mFileName);
		if (mListConfig.isInCategoryMode() || mParentId >= 0)
			l.setCategoryRootId(mParentId);
		mListLoader = l;
		return l;
	}

	@Override
	public void onLoadFinished(Loader<List<ListItemInfo>> loader,
			List<ListItemInfo> data) {
		// Logger.DoLog("onLoadFinished", LogType.Debug);
		mlistItems = data;
		mListAdapter.resetData(data);
		// The list should now be shown.
		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
		setLoadingShown(false);
		// MasterActivity.updateListViewHeight((ListView) mListView);
	}

	@Override
	public void onLoaderReset(Loader<List<ListItemInfo>> loader) {
		// if (AppConfig.DeviceScreenType == ScreenType.Phone)
		mListAdapter.resetData(null);
		// else
		// mlistItems = null;
	}

	public boolean onQueryTextChange(String query) {
		mQuery = TextUtils.isEmpty(query) ? null : query;
		getLoaderManager().restartLoader(mLoaderId, null, this);
		return true;
	}

	public void setListShown(boolean shown) {
		if (mListView != null)
			mListView.setVisibility(shown ? View.VISIBLE : View.GONE);
	}

	public void setListShownNoAnimation(boolean shown) {
		// show with no animation should be implemented.
		setListShown(shown);
	}

	protected void setLoadingShown(boolean shown) {
		if (!shown) {
			prgListLoader.setVisibility(View.GONE);
			prgListLoader.clearAnimation();
			if (mlistItems == null || mlistItems.size() == 0)
				emptyView.setVisibility(View.VISIBLE);
		} else {
			prgListLoader.setVisibility(View.VISIBLE);
			prgListLoader.startAnimation(PLConfig.anim_syncer_rotate());
			emptyView.setVisibility(View.GONE);
		}
	}

	public int getFileId() {
		return mFileId;
	}

	public ListConfig getListConfig() {
		return mListConfig;
	}

	public AbsListView getListView() {
		return mListView;
	}
}
