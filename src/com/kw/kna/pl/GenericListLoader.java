package com.kw.kna.pl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.kw.kna.R;
import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;
import com.kw.kna.bus.enums.DataType;
import com.kw.kna.io.FileServices;
import com.kw.kna.io.JSONItemsLexicon;
import com.kw.kna.io.ParsedData;
import com.kw.kna.io.ParsedObject;

public class GenericListLoader extends AsyncTaskLoader<List<ListItemInfo>> {

	private List<ListItemInfo> mData;
	private int mFileId;
	private String mQueryLower;// , mQueryUpper;
	private int mParentId = -1;
	private String mFileName;
	private String itemSection;
	private ListItemInfo tmpItem;
	private List<ListItemInfo> mTopNews;

	public GenericListLoader(Context context, int fileId, String query) {
		super(context);
		mFileId = fileId;
		if (query == null) {
			mQueryLower = query;
			// mQueryUpper = query;
		} else {
			mQueryLower = query.toLowerCase(Locale.ENGLISH);
			// mQueryUpper = query.toUpperCase(Locale.ENGLISH);
		}
	}

	public GenericListLoader clone() {
		GenericListLoader loader = new GenericListLoader(getContext(), mFileId,
				mQueryLower);
		loader.mFileId = this.mFileId;
		loader.mFileName = this.mFileName;
		loader.mParentId = this.mParentId;
		loader.mQueryLower = this.mQueryLower;
		// loader.mQueryUpper = this.mQueryUpper;
		return loader;
	}

	public void setFileName(String mFileName) {
		this.mFileName = mFileName;
	}

	public String getQueryLower() {
		return mQueryLower;
	}

	public void setCategoryRootId(int rootId) {
		mParentId = rootId;
	}

	public int getFileId() {
		return mFileId;
	}

	public int getParentId() {
		return mParentId;
	}

	@Override
	public List<ListItemInfo> loadInBackground() {
		return loadData(mFileId, mFileName);
	}

	public List<ListItemInfo> loadData() {
		return loadData(mFileId, null);
	}

	public List<ListItemInfo> loadData(int fileId, String fileName) {
		try {
			ParsedObject[] parsedObjs;
			mData = new ArrayList<ListItemInfo>();
			// String itemName;
			int itemParentId;
			switch (fileId) {
			case R.string.fj_lib:
				parsedObjs = readJsonData(fileId, fileName);
				readDataItemsForSection(parsedObjs, PLConfig.section_Documents);
				break;
			case R.string.fj_members_list:
				parsedObjs = readJsonData(fileId, fileName);
				readDataItemsForSection(parsedObjs, PLConfig.section_Members);
				break;
			case R.string.fj_about:
				parsedObjs = readJsonData(fileId, fileName);
				readDataItemsForSection(parsedObjs, PLConfig.section_About);
				break;
			case R.string.fj_gallery_list:
				parsedObjs = readJsonData(fileId, fileName);
				readDataItemsForAlbumSection(parsedObjs);
				break;
			case R.string.fj_constitution:
				parsedObjs = readJsonData(fileId, fileName);
				readDataItemsForSection(parsedObjs,
						PLConfig.section_Constitution);
				break;
			case R.string.fj_memo:
				parsedObjs = readJsonData(fileId, fileName);
				readDataItemsForSection(parsedObjs, PLConfig.section_Memo);
				break;
			case R.string.fj_member_items:
				parsedObjs = readJsonData(fileId, fileName);
				ParsedObject tmpObj = null;
				for (int i = 0; i < parsedObjs.length; i++) {
					itemParentId = (Integer) parsedObjs[i].getItems().get(
							JSONItemsLexicon.id);
					if (itemParentId == mParentId) {
						tmpObj = parsedObjs[i];
						break;
					}
				}
				if (tmpObj == null)
					break;
				tmpObj = (ParsedObject) tmpObj.getItems().get(
						JSONItemsLexicon.members);
				// tmpObj = (ParsedObject) tmpObj.getChilds()[0];
				ParsedObject tmpChildObj;
				String cv;
				for (int i = 0; i < tmpObj.getChilds().length; i++) {
					tmpChildObj = (ParsedObject) tmpObj.getChilds()[i];
					cv = tmpChildObj.getStringItem(JSONItemsLexicon.cv);
					tmpItem = new ListItemInfo(mParentId,
							tmpChildObj.getStringItem(JSONItemsLexicon.name),
							tmpChildObj.getStringItem(JSONItemsLexicon.title),
							tmpChildObj.getStringItem(JSONItemsLexicon.photo),
							cv != null && cv.length() > 0, mParentId);
					tmpItem.setTag(cv);
					mData.add(tmpItem);
				}
				break;
			case R.string.fj_news:
				com.kw.kna.io.rss.RSSHandler rh = new com.kw.kna.io.rss.RSSHandler();
				List<com.kw.kna.io.rss.RSSItem> items = rh
						.getLatestArticles(AppConfig.WQS_NewsRSS);
				mTopNews = new ArrayList<ListItemInfo>();
				for (int i = 0; i < items.size(); i++) {
					tmpItem = new ListItemInfo(i, items.get(i).getTitle(),
							GeneralPLS.PrepareNewsDateForList(items.get(i)
									.getPubDate()), items.get(i)
									.getVerticalImage(), true, R.string.fj_news);
					tmpItem.setTag(items.get(i).getDescription());
					tmpItem.addExtra(R.id.itemExtra_hImage, items.get(i)
							.getHorizontalImage());
					if (items.get(i).isPriority() && mTopNews.size() < 10) {
						mTopNews.add(tmpItem);
					} else
						mData.add(tmpItem);
				}
				break;
			}
			return mData;
		} catch (Exception e) {
			Logger.DoLog("load in backgroud failed!", e.getMessage(), this
					.getClass().getName(), LogType.Error);
			return null;
		}
	}

	private ParsedObject[] readJsonData(int fileId, String fileName) {
		ParsedData parsedData;
		if (fileName != null)
			parsedData = FileServices.readJSONFile(fileId, fileName);
		else
			parsedData = FileServices.readJSONFile(fileId);
		List<ParsedObject> allItems = new ArrayList<ParsedObject>();
		if (parsedData != null && parsedData.isSuccess()
				&& parsedData.getObjects() != null)
			Collections.addAll(allItems, parsedData.getObjects());
		return allItems.toArray(new ParsedObject[allItems.size()]);
	}

	private void readDataItemsForSection(ParsedObject[] parsedObjs,
			String section) {
		String itemName, file;
		for (int i = 0; i < parsedObjs.length; i++) {
			itemSection = parsedObjs[i].getStringItem(JSONItemsLexicon.section);
			if (!section.equals(itemSection))
				continue;
			itemName = parsedObjs[i].getStringItem(JSONItemsLexicon.title);
			if (!checkQuery(itemName))
				continue;
			tmpItem = new ListItemInfo((Integer) parsedObjs[i].getItems().get(
					JSONItemsLexicon.id), itemName, null, null, true);
			tmpItem.setTag(DataType.fromValue(parsedObjs[i]
					.getStringItem(JSONItemsLexicon.type)));
			file = parsedObjs[i].getStringItem(JSONItemsLexicon.file);
			if (file != null && file.length() > 0)
				tmpItem.addExtra(R.id.itemExtra_file, file);
			mData.add(tmpItem);
		}
	}

	private void readDataItemsForAlbumSection(ParsedObject[] parsedObjs) {
		// boolean isAlbum = section.equals(PLConfig.section_Albums);
		ParsedObject imagesObj;
		String albumImage;
		for (int i = 0; i < parsedObjs.length; i++) {
			itemSection = parsedObjs[i].getStringItem(JSONItemsLexicon.section);
			if (!PLConfig.section_Albums.equals(itemSection))
				continue;
			albumImage = null;
			if ((imagesObj = (ParsedObject) parsedObjs[i].getItems().get(
					JSONItemsLexicon.images)) != null
					&& imagesObj.isArray() && imagesObj.getChilds().length > 0) {
				albumImage = (String) imagesObj.getChilds()[0];
			}
			tmpItem = new ListItemInfo((Integer) parsedObjs[i].getItems().get(
					JSONItemsLexicon.id),
					parsedObjs[i].getStringItem(JSONItemsLexicon.title),
					parsedObjs[i].getStringItem(JSONItemsLexicon.year),
					albumImage, true);
			tmpItem.setTag(DataType.fromValue(parsedObjs[i]
					.getStringItem(JSONItemsLexicon.type)));
			mData.add(tmpItem);
		}
	}

	public List<ListItemInfo> getTopNews() {
		return mTopNews;
	}

	private boolean checkQuery(String itemName) {
		return mQueryLower == null
				|| itemName.toLowerCase(Locale.ENGLISH).contains(mQueryLower);
		// || itemName.toUpperCase(Locale.ENGLISH).contains(mQueryUpper);
	}

	@Override
	public void deliverResult(List<ListItemInfo> data) {
		if (isReset()) {
			// The Loader has been reset; ignore the result and invalidate the
			// data.
			releaseResources(data);
			return;
		}

		// Hold a reference to the old data so it doesn't get garbage collected.
		// We must protect it until the new data has been delivered.
		List<ListItemInfo> oldData = mData;
		mData = data;

		if (isStarted()) {
			// If the Loader is in a started state, deliver the results to the
			// client. The superclass method does this for us.
			super.deliverResult(data);
		}

		// Invalidate the old data as we don't need it any more.
		if (oldData != null && oldData != data) {
			releaseResources(oldData);
		}
	}

	@Override
	protected void onStartLoading() {
		if (mData != null) {
			// Deliver any previously loaded data immediately.
			deliverResult(mData);
		}

		if (takeContentChanged() || mData == null) {
			// When the observer detects a change, it should call
			// onContentChanged()
			// on the Loader, which will cause the next call to
			// takeContentChanged()
			// to return true. If this is ever the case (or if the current data
			// is
			// null), we force a new load.
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading() {
		// The Loader is in a stopped state, so we should attempt to cancel the
		// current load (if there is one).
		cancelLoad();
		// Note that we leave the observer as is. Loaders in a stopped state
		// should still monitor the data source for changes so that the Loader
		// will know to force a new load if it is ever started again.
	}

	@Override
	protected void onReset() {
		// Ensure the loader has been stopped.
		onStopLoading();
		// At this point we can release the resources associated with 'mData'.
		if (mData != null) {
			releaseResources(mData);
			mData = null;
		}
	}

	@Override
	public void onCanceled(List<ListItemInfo> data) {
		// Attempt to cancel the current asynchronous load.
		super.onCanceled(data);
		// The load has been canceled, so we should release the resources
		// associated with 'data'.
		releaseResources(data);
	}

	private void releaseResources(List<ListItemInfo> data) {
		// For a simple List, there is nothing to do. For something like a
		// Cursor, we
		// would close it in this method. All resources associated with the
		// Loader
		// should be released here.
		data = null;
		System.gc();
	}
}
