package com.kw.kna.pl;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kw.kna.R;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;
import com.kw.kna.bus.enums.AsyncOperationResult;
import com.kw.kna.bus.enums.AsyncOperationType;
import com.kw.kna.io.AsyncOperationTask;
import com.kw.kna.io.FileServices;
import com.kw.kna.io.OnAsyncOperationListener;

public class FragmentPDFView extends Fragment implements
		OnAsyncOperationListener, OnPageChangeListener {

	private PDFPageLoader mPDFLoader;
	private String mUrl;
	private String mFilePath;
	private AsyncOperationTask asyncTask;
	private ProgressBar progUpdateData;
	private ViewPager vFlipper;
	private TextView txtCurrentPage;
	private int currentIndex;
	private View rowPager;
	private TextView txtProgress;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mUrl = getArguments().getString(IntentExtraLexicon.Url);
		mFilePath = getArguments().getString(IntentExtraLexicon.FileName);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_doc_view, null);
		vFlipper = (ViewPager) v.findViewById(R.id.vfSlider);
		rowPager = v.findViewById(R.id.layPager);
		txtCurrentPage = (TextView) v.findViewById(R.id.txtCurrentPage);
		progUpdateData = (ProgressBar) v.findViewById(R.id.progUpdateData);
		txtProgress = (TextView) v.findViewById(R.id.txt_progress);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		loadData();
	}

	private void loadData() {
		try {
			if (!FileServices.checkFileExist(mFilePath)) {
				asyncTask = new AsyncOperationTask(this,
						AsyncOperationType.GetPDF);
				asyncTask.executeOnParallel(new Object[] { mFilePath, mUrl });
				progUpdateData.setVisibility(View.VISIBLE);
				txtProgress.setVisibility(View.VISIBLE);
				progUpdateData.startAnimation(PLConfig.anim_syncer_rotate());
			} else {
				mPDFLoader = new PDFPageLoader(mFilePath);
				loadPDFPages();
			}
		} catch (Exception e) {
			Logger.DoLog("load data failed!", e.getMessage(), this.getClass()
					.getName(), LogType.Error);
		}

	}

	@Override
	public void onDone(AsyncOperationType type, AsyncOperationResult result,
			Object response) {
		try {
			switch (type) {
			case GetPDF:
				progUpdateData.setVisibility(View.GONE);
				txtProgress.setVisibility(View.GONE);
				progUpdateData.clearAnimation();
				GeneralPLS.handleGeneralAsyncOperationResult(result,
						getActivity(), false);
				if (result == AsyncOperationResult.Success) {
					mPDFLoader = new PDFPageLoader(mFilePath);
					loadPDFPages();
					// load the html in the webview
					// wViewer.getSettings().setBuiltInZoomControls(true);
					// wViewer.loadDataWithBaseURL("", (String) response,
					// "text/html",
					// "UTF-8", "");
				}
				break;

			default:
				break;
			}
		} catch (Exception e) {
			Logger.DoLog("handle async task response failed!", e.getMessage(),
					LogType.Error);
			GeneralPLS.showFailureToast(getActivity());
		}
	}

	@Override
	public void onProgress(AsyncOperationType type, int progress) {
		switch (type) {
		case GetPDF:
			txtProgress.setText(progress + "%");
			break;
		default:
			break;
		}
	}

	private void loadPDFPages() {
		try {
			if (!mPDFLoader.isInitialized() || mPDFLoader.getPageCount() == 0) {
				vFlipper.setVisibility(View.GONE);
				return;
			}
			vFlipper.removeAllViews();
			vFlipper.setOnPageChangeListener(this);

			PagerAdapter adapter = new PagerAdapter() {

				@Override
				public boolean isViewFromObject(View v, Object obj) {
					return v == (View) obj;
				}

				@Override
				public Object instantiateItem(ViewGroup container, int position) {
					TouchImageView img = new TouchImageView(getActivity());
					mPDFLoader.load(position, img);
					((ViewPager) container).addView(img);
					return img;
				}

				@Override
				public int getCount() {
					return mPDFLoader.getPageCount();
				}

				@Override
				public void destroyItem(ViewGroup container, int position,
						Object object) {
					if (object instanceof ImageView)
						((ImageView) object).destroyDrawingCache();
					container.removeView((View) object);
					System.gc();
				}
			};
			vFlipper.setAdapter(adapter);
			vFlipper.setCurrentItem(0);
			currentIndex = 0;
			if (mPDFLoader.getPageCount() > 1) {
				rowPager.setVisibility(View.VISIBLE);
				txtCurrentPage.setText((currentIndex + 1) + "");
				((TextView) getView().findViewById(R.id.txtTotalPage))
						.setText(mPDFLoader.getPageCount() + "");
			}

		} catch (Exception e) {
			Logger.DoLog("load pdf pages failed!", e.getMessage(),
					LogType.Error);
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int index) {
		currentIndex = index;
		setFlipperPage();
	}

	private void setFlipperPage() {
		txtCurrentPage.setText((currentIndex + 1) + "");
	}
}
