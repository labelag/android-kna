package com.kw.kna.pl;

public interface OnListItemClickListener {
	public abstract void onListItemClick(ListItemInfo itemInfo, int index);
}
