package com.kw.kna.pl;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.artifex.mupdfdemo.ChoosePDFActivity;
import com.artifex.mupdfdemo.MuPDFActivity;
import com.kw.kna.MainActivity;
import com.kw.kna.R;
import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.enums.AsyncOperationResult;
import com.kw.kna.bus.enums.AsyncOperationType;
import com.kw.kna.bus.enums.DataType;
import com.kw.kna.io.AsyncOperationTask;
import com.kw.kna.io.OnAsyncOperationListener;

public class TitledListFragment extends ListFragment implements
		OnAsyncOperationListener {

	private AsyncOperationTask asyncTask;
	private ProgressBar prgUpdateData;
	private boolean mIsDataUpdated;

	public android.view.View onCreateView(android.view.LayoutInflater inflater,
			android.view.ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.titled_list, null);
		// if (AppConfig.DeviceScreenType == ScreenType.Phone) {
		mListView = (AbsListView) v.findViewById(android.R.id.list);
		mListView.setOnItemClickListener(this);
		mListView.setAdapter(mListAdapter);
		// } else {
		// mTableView = (TableLayout) v.findViewById(R.id.tblGeneral);
		// }
		emptyView = v.findViewById(android.R.id.empty);
		prgListLoader = v.findViewById(android.R.id.progress);

		return v;
	};

	@Override
	public void onStart() {
		super.onStart();
		prepareTitle();
		if (mIsDataUpdated)
			return;
		AsyncOperationType opType = AsyncOperationType.None;
		switch (mListConfig.getFileId()) {
		case R.string.fj_lib:
		case R.string.fj_members_list:
		case R.string.fj_gallery_list:
		case R.string.fj_about:
		case R.string.fj_member_items:
		case R.string.fj_doc_detail:
		case R.string.fj_gallety_items:
		case R.string.fj_constitution:
		case R.string.fj_memo:
			opType = AsyncOperationType.UpdateDocuments;
			break;
		default:
			// opType = AsyncOperationType.LiveUpdate;
			break;
		}
		((MainActivity) getActivity()).setTabMenuSelected(mFileId);
		if (opType != AsyncOperationType.None) {
			asyncTask = new AsyncOperationTask(this, opType);
			asyncTask.executeOnParallel(new Object[0]);
			prgUpdateData = (ProgressBar) getView().findViewById(
					R.id.progUpdateData);
			prgUpdateData.setVisibility(View.VISIBLE);
		}
		mIsDataUpdated = true;
	}

	@Override
	public void onDone(AsyncOperationType type, AsyncOperationResult result,
			Object response) {
		switch (type) {
		case UpdateDocuments:
		case UpdateNewsList:
			// case UpdateDocDetail:
			prgUpdateData.setVisibility(View.GONE);
			// getActivity().setProgressBarVisibility(false);
			GeneralPLS.handleGeneralAsyncOperationResult(result, getActivity(),
					false);
			if (result == AsyncOperationResult.Success)
				getLoaderManager().restartLoader(mLoaderId, null, this);
			break;
		default:
			break;
		}

	}

	@Override
	public void onProgress(AsyncOperationType type, int progress) {

	}

	@Override
	public void onDestroy() {
		if (asyncTask != null)
			asyncTask.cancel(true);
		super.onDestroy();
	}

	private void prepareTitle() {
		switch (mFileId) {
		case R.string.fj_member_items:

			if (mListConfig.getTabTitle() != null
					&& mListConfig.getTabTitle().length() > 0) {
				((TextView) getView().findViewById(R.id.txtMembersTitle))
						.setText(mListConfig.getTabTitle());
				getView().findViewById(R.id.layMembersTitle).setVisibility(
						View.VISIBLE);
			}
			break;
		}
	}

	@Override
	protected void handleItemClick(int index) {
		ListItemInfo item = null;
		switch (mFileId) {
		case R.string.fj_lib:
		case R.string.fj_members_list:
		case R.string.fj_about:
		case R.string.fj_gallery_list:
		case R.string.fj_constitution:
		case R.string.fj_memo:
			item = mlistItems.get(index);
			DataType type = (DataType) item.getTag();

			switch (type) {
			case document:
				String fileUrl = (String) item.getExtras().get(
						R.id.itemExtra_file);
				String filePath;
				try {
					filePath = AppConfig.DataFullPath
							+ item.getId()
							+ "/"
							+ URLEncoder.encode(new File(fileUrl).getName(),
									PLConfig.Charset_UTF8);
				} catch (UnsupportedEncodingException e) {
					filePath = AppConfig.DataFullPath + item.getId() + "/"
							+ new File(fileUrl).getName().hashCode();
				}
				((MainActivity) getActivity()).showPDFViewerFragment(filePath,
						fileUrl);
				break;
			case gallery_list:
				// default:
				((MainActivity) getActivity()).showDocViewFragment(
						item.getId(), mFileId);
				break;
			case members_list:
				ListConfig lstConfig = new ListConfig(item.getText(),
						R.string.fj_member_items);
				lstConfig.setItemViewResourceId(R.layout.members_list_item);
				lstConfig.setParentId(item.getId());
				((MainActivity) getActivity())
						.showAutoUpdateFragment(lstConfig);
				((MainActivity) getActivity()).showBackAction(true);
				break;
			case Static:
			case Url:
				((MainActivity) getActivity()).showStaticViewFragment(item
						.getId());
				((MainActivity) getActivity()).setTabMenuSelected(mFileId);
				break;
			}

			break;
		case R.string.fj_member_items:
			item = mlistItems.get(index);
			if (item.getShowIndicator()) {
				String filePath = AppConfig.DataFullPath
						+ AppConfig.CVDirsPrefix + item.getId() + "/"
						+ new File((String) item.getTag()).getName();
				
				((MainActivity) getActivity()).showPDFViewerFragment(filePath,
						(String) item.getTag());
			}
			break;
		case R.string.fj_news:
			item = mlistItems.get(index);
			String image;
			if (item.getExtras().get(R.id.itemExtra_hImage) != null)
				image = (String) item.getExtras().get(R.id.itemExtra_hImage);
			else
				image = item.getImage();
			((MainActivity) getActivity()).showNewsDetailFragment(item.getId(),
					item.getText(), (String) item.getTag(), image,
					item.getDescription());
			break;
		default:
			super.handleItemClick(index);
			break;
		}
		if (mListConfig.getOnClicklistener() != null) {
			mListConfig.getOnClicklistener().onListItemClick(item, index);
		}
	}
}
