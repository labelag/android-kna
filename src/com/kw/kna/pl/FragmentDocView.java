package com.kw.kna.pl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.kw.kna.MainActivity;
import com.kw.kna.R;
import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;
import com.kw.kna.io.FileServices;
import com.kw.kna.io.JSONItemsLexicon;
import com.kw.kna.io.ParsedData;
import com.kw.kna.io.ParsedObject;

public class FragmentDocView extends Fragment implements OnPageChangeListener {

	private ViewPager vFlipper;
	private String[] adImageFiles;
	private final SDImageLoader mImageLoader = new SDImageLoader();
	private int currentIndex;
	private View rowPager;
	private int mDocId;
	private TextView txtCurrentPage;
	private int mFileId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mDocId = getArguments().getInt(IntentExtraLexicon.Id);
		mFileId = getArguments().getInt(IntentExtraLexicon.FileId);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_doc_view, null);
		vFlipper = (ViewPager) v.findViewById(R.id.vfSlider);
		rowPager = v.findViewById(R.id.layPager);
		txtCurrentPage = (TextView) v.findViewById(R.id.txtCurrentPage);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		((MainActivity) getActivity()).setTabMenuSelected(mFileId);
		loadAdDetails();
	}

	@Override
	public void onStart() {
		super.onStart();

	}

	private void loadAdDetails() {
		try {
			ParsedData data = FileServices.readJSONFile(R.string.fj_doc_detail);
			if (data == null)
				return;
			if (!data.isSuccess() || data.getObjects() == null)
				throw new Exception("data is invalid or parse data failed!");
			// if (objs.length != 1)
			// throw new Exception("more than on ad on json!");
			ParsedObject docObj = null;
			for (int i = 0; i < data.getObjects().length; i++) {
				if ((Integer) data.getObjects()[i].getItems().get(
						JSONItemsLexicon.id) == mDocId) {
					docObj = data.getObjects()[i];
					break;
				}
			}
			if (docObj == null) {
				GeneralPLS.showToast(R.string.m_item_not_found);
				return;
			}
			((TextView) getView().findViewById(R.id.txtTotalPage))
					.setText(docObj.getItems()
							.get(JSONItemsLexicon.number_of_page).toString());
			txtCurrentPage.setText((currentIndex + 1) + "");

			// region --------------- check modification ----------------
			/*
			 * check last modification of doc and if updated, the older doc
			 * images will be deleted.
			 */

			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
					Locale.ENGLISH);
			Date dt = df.parse(docObj.getStringItem(JSONItemsLexicon.date));
			File f = new File(AppConfig.DataFullPath + mDocId);
			if (f.exists() && dt.getTime() > f.lastModified())
				f.delete();

			// endregion -------------------- check modification

			loadAdPics((ParsedObject) docObj.getItems().get(
					JSONItemsLexicon.images));
		} catch (Exception e) {
			Logger.DoLog("load ad details failed!", e.getMessage(),
					LogType.Error);
			GeneralPLS.showFailureToast(getActivity());
		}
	}

	private void loadAdPics(ParsedObject picsObj) {
		try {
			if (picsObj == null || picsObj.isArray() == false
					|| picsObj.getChilds().length == 0) {
				vFlipper.setVisibility(View.GONE);
				return;
			}
			vFlipper.removeAllViews();
			vFlipper.setOnPageChangeListener(this);

			TableRow.LayoutParams pagerLayout = new TableRow.LayoutParams(
					TableRow.LayoutParams.WRAP_CONTENT,
					TableRow.LayoutParams.WRAP_CONTENT);
			// rowPager.removeAllViews();
			pagerLayout.setMargins(0, 0, 3, 0);
			adImageFiles = new String[picsObj.getChilds().length];
			// mWebClient = new MyWebViewClient();
			for (int i = 0; i < adImageFiles.length; i++) {
				adImageFiles[i] = (String) picsObj.getChilds()[i];
			}
			PagerAdapter adapter = new PagerAdapter() {

				@Override
				public boolean isViewFromObject(View v, Object obj) {
					return v == (View) obj;
				}

				@Override
				public Object instantiateItem(ViewGroup container, int position) {
					TouchImageView img = new TouchImageView(getActivity());
					// String fullPath = AppConfig.ImagesFullPath
					// + adImageFiles[position];
					img.setTag(adImageFiles[position]);
					mImageLoader.load(AppConfig.ImagesFullPath + mDocId + "/"
							+ new File(adImageFiles[position]).getName(), img,
							false);
					// img.setOnClickListener(getListener());
					// WebView wv = new WebView(getActivity());
					// wv.getSettings().setBuiltInZoomControls(true);
					// wv.getSettings().setLoadWithOverviewMode(true);
					// wv.setTag(adImageFiles[position]);
					// mImageLoader.load(AppConfig.ImagesFullPath + mDocId + "/"
					// + new File(adImageFiles[position]).getName(), wv,
					// false);
					// wv.loadUrl(AppConfig.ImagesFullPath + mDocId + "/"
					// + new File(adImageFiles[position]).getName());
					((ViewPager) container).addView(img);
					return img;
				}

				@Override
				public int getCount() {
					return adImageFiles.length;
				}

				@Override
				public void destroyItem(ViewGroup container, int position,
						Object object) {
					if (object instanceof ImageView)
						((ImageView) object).destroyDrawingCache();
					// if (container.getChildCount() > position)
					// container.removeViewAt(position);
					container.removeView((View) object);
					System.gc();
				}
			};
			vFlipper.setAdapter(adapter);
			vFlipper.setCurrentItem(0);
			currentIndex = 0;
			if (adImageFiles.length > 1)
				rowPager.setVisibility(View.VISIBLE);

		} catch (Exception e) {
			Logger.DoLog("load ad pics failed!", e.getMessage(), LogType.Error);
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int arg0) {
		setFlipperPage();
	}

	private void setFlipperPage() {
		// imgPages[currentIndex]
		// .setImageResource(R.drawable.slider_control_inactive);
		currentIndex = vFlipper.getCurrentItem();
		txtCurrentPage.setText((currentIndex + 1) + "");
		// imgPages[currentIndex]
		// .setImageResource(R.drawable.slider_control_active);
	}

	public int getFileId() {
		return mFileId;
	}
}
