package com.kw.kna.pl;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

import com.kw.kna.bus.AppConfig;

public class PLConfig {

	public static final int Data_Page_Size = 40;

	public static final String section_About = "About";
	public static final String section_Albums = "Albums";
	public static final String section_Documents = "Documents";
	public static final String section_Members = "Members";
	public static final String section_Constitution = "Constitution";
	public static final String section_Memo = "Memo";

	public static final int List_Thumb_Radiuspx = 15;

	public static final String Charset_UTF8 = "utf-8";

	public static int ScreenWidth;
	public static int ScreenHeight;

	// public static RotateAnimation anim_syncer_rotate;
	// public static TranslateAnimation anim_syncer_transform;
	// public static AlphaAnimation anim_syncer_alpha;

	public static void init() {
		Display display = ((WindowManager) AppConfig.Application
				.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point dispSize = new Point();
		display.getSize(dispSize);
		// DisplayMetrics displayMetrics = new DisplayMetrics();
		ScreenWidth = dispSize.x;
		ScreenHeight = dispSize.y;
		// Logger.DoLog("size:" + ScreenWidth + "," + ScreenHeight,
		// LogType.Info);

		// anim_syncer_rotate = new RotateAnimation(0f, 360f,
		// Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
		// 0.5f);
		// anim_syncer_rotate.setDuration(1000);
		// anim_syncer_rotate.setRepeatMode(Animation.RESTART);
		// anim_syncer_rotate.setRepeatCount(Animation.INFINITE);
		//
		// anim_syncer_transform = new TranslateAnimation(
		// Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
		// Animation.RELATIVE_TO_SELF, -100, Animation.RELATIVE_TO_SELF, 0);
		// anim_syncer_transform.setDuration(2000);
		// anim_syncer_transform.setRepeatMode(Animation.RESTART);
		// anim_syncer_transform.setRepeatCount(Animation.INFINITE);
		//
		// anim_syncer_alpha = new AlphaAnimation(0.0f, 1.0f);
		// anim_syncer_alpha.setDuration(3000);
		// anim_syncer_alpha.setRepeatMode(Animation.RESTART);
		// anim_syncer_alpha.setRepeatCount(Animation.INFINITE);
	}

	public static RotateAnimation anim_syncer_rotate() {
		RotateAnimation anim = new RotateAnimation(0f, 360f,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		anim.setDuration(1000);
		anim.setRepeatMode(Animation.RESTART);
		anim.setRepeatCount(Animation.INFINITE);
		return anim;
	}
}
