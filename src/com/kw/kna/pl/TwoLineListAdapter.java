package com.kw.kna.pl;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kw.kna.R;
import com.kw.kna.bus.AppConfig;

public class TwoLineListAdapter extends ArrayAdapter<ListItemInfo> {

	private List<ListItemInfo> items;
	private int mDescMaxLine = 1;
	private boolean mShowIndicators;
	private int textViewResourceId = R.layout.list_item_twoline_dark;
	private int indicatorResourceId;
	private final SDImageLoader mImageLoader = new SDImageLoader();
	// private BitmapDrawable bgDrawable;
	private boolean thumbnailVisible = true;

	public TwoLineListAdapter(Context cnt) {
		super(cnt, R.layout.list_item_twoline_dark, R.id.txtItemText);
		items = new ArrayList<ListItemInfo>();
	}

	public TwoLineListAdapter(Context cnt, ListItemInfo[] objects) {
		super(cnt, R.layout.list_item_twoline_dark, R.id.txtItemText, objects);
		items = Arrays.asList(objects);
	}

	public TwoLineListAdapter(Context cnt, List<ListItemInfo> objects) {
		super(cnt, R.layout.list_item_twoline_dark, R.id.txtItemText, objects);
		items = objects;
	}

	public void setRoundedCornerThumbs(boolean rounded) {
		mImageLoader.setRoundedCorner(rounded);
	}

	public void setDescMaxLine(int maxLine) {
		mDescMaxLine = maxLine;
	}

	public void setShowIndicators(boolean show) {
		mShowIndicators = show;
	}

	public void setTextViewResourceId(int textViewResourceId) {
		this.textViewResourceId = textViewResourceId;
	}

	public void setIndicatorResourceId(int indicatorResourceId) {
		this.indicatorResourceId = indicatorResourceId;
	}

	public void setThumbnailVisible(boolean thumbnailVisible) {
		this.thumbnailVisible = thumbnailVisible;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		// if (v == null) {
		v = ((Activity) getContext()).getLayoutInflater().inflate(
				textViewResourceId, null);
		// }
		ListItemInfo o = items.get(position);
		if (o != null) {
			TextView txt = (TextView) v.findViewById(R.id.txtItemText);
			if (txt != null) {
				txt.setText(o.getText());
			}

			txt = (TextView) v.findViewById(R.id.txtItemDesc);
			if (txt != null) {
				txt.setText(o.getDescription());
				txt.setMaxLines(mDescMaxLine);
			}

			txt = (TextView) v.findViewById(R.id.txtItemExtras);
			if (txt != null && o.getExtras().size() > 0) {
				String extra = o.getExtras().get(o.getExtras().keyAt(0)) + "";
				for (int i = 1; i < o.getExtras().size(); i++) {
					extra += "  |  "
							+ o.getExtras().get(o.getExtras().keyAt(i));
				}
				txt.setText(extra);
			}

			ImageView img = (ImageView) v.findViewById(R.id.imgItemIcon);

			if (img != null) {
				if (!thumbnailVisible)
					img.setVisibility(View.GONE);
				else if (o.getImage() != null) {
					// if (o.getTag() instanceof DataType
					// && (DataType) o.getTag() == DataType.gallery_list) {
					img.setTag(o.getImage());
					String path = AppConfig.ImagesFullPath;
					if (o.getParentId() == R.string.fj_news)
						path += AppConfig.NewsImagesDir;
					else
						path += o.getId() + "/";
					path += new File(o.getImage()).getName();
					mImageLoader.load(path, img);
					// } else
					// mImageLoader.load(
					// AppConfig.ImagesFullPath + o.getImage(), img);
				}
			}

			img = (ImageView) v.findViewById(R.id.imgArrowIcon);
			if (img != null) {
				if (!mShowIndicators && !o.getShowIndicator()) {
					img.setVisibility(View.INVISIBLE);
				} else if (indicatorResourceId > 0)
					img.setImageResource(indicatorResourceId);
			}
			// if (bgDrawable != null)
			// v.setBackground(bgDrawable);
			if (!o.isEnabled())
				v.setAlpha(0.6f);
		}
		return v;
	}

	@Override
	public boolean isEnabled(int position) {
		if (items != null || items.size() > position)
			return items.get(position).isEnabled();
		return false;
	}

	@Override
	public long getItemId(int position) {
		if (items == null || items.size() <= position)
			return -1;
		return items.get(position).getId();
	}

	public void resetData(List<ListItemInfo> data) {
		super.clear();
		this.items = data;
		if (data != null)
			super.addAll(data);
		// if (this.items != null) {
		// for (int i = 0; i < items.size(); i++) {
		// super.insert(items.get(i), i);
		// }
		// }
		this.notifyDataSetChanged();
	}

	@Override
	public void add(ListItemInfo object) {
		if (object == null)
			return;
		this.items.add(object);
		super.add(object);
	}

	@Override
	public void addAll(ListItemInfo... collection) {
		if (collection == null)
			return;
		Collections.addAll(items, collection);
		super.addAll(collection);
	}

	@Override
	public void addAll(Collection<? extends ListItemInfo> collection) {
		if (collection == null)
			return;
		items.addAll(collection);
		super.addAll(collection);
	}

}
