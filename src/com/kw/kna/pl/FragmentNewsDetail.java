package com.kw.kna.pl;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.kw.kna.MainActivity;
import com.kw.kna.R;
import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;

public class FragmentNewsDetail extends Fragment {

	// private int mResId;
	private WebView wviewDesc;
	private String mTitle;
	private String mDesc;
	private String mImage;
	private String mDate;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// mResId = getArguments().getInt(IntentExtraLexicon.Id);
		mTitle = getArguments().getString(IntentExtraLexicon.Title);
		mDesc = getArguments().getString(IntentExtraLexicon.Desc);
		mImage = getArguments().getString(IntentExtraLexicon.Image);
		mDate = getArguments().getString(IntentExtraLexicon.Date);

		Pattern p = Pattern.compile("((<img).*(/>))");
		Matcher m = p.matcher(mDesc);
		if (m.find()) {
			for (int i = 0; i < m.groupCount(); i++) {
				mDesc = mDesc.replace(m.group(i), "");
			}
		}
		mDesc = "<div style=\"direction:rtl;\">" + mDesc + "</div>";
		// GeneralPLS.showToast("News : " + mResId);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_news_detail, null);

		// progUpdateData = (ProgressBar) v.findViewById(R.id.progUpdateData);

		return v;
	}

	@Override
	public void onStart() {
		super.onStart();
		try {
			((MainActivity) getActivity())
					.setTabMenuSelected(R.string.fj_news_detail);
			((TextView) getView().findViewById(R.id.txt_title)).setText(mTitle);
			((TextView) getView().findViewById(R.id.txt_date)).setText(mDate);
			ImageView img = (ImageView) getView().findViewById(
					R.id.img_news_image);
			if (mImage != null && mImage.length() > 0) {
				img.setTag(mImage);
				new SDImageLoader().load(AppConfig.ImagesFullPath+AppConfig.NewsImagesDir
						+ new File(mImage).getName(), img, false);
			}

			wviewDesc = (WebView) getView().findViewById(R.id.wviewDescription);
			wviewDesc.getSettings().setDefaultTextEncodingName("utf-8");
			wviewDesc.loadData(mDesc, "text/html; charset=utf-8", "utf-8");
			if (Build.VERSION.SDK_INT >= 11) {
				wviewDesc.setBackgroundColor(0x01FFFFFF);
			} else {
				wviewDesc.setBackgroundColor(0x00FFFFFF);
			}
		} catch (Exception e) {
			GeneralPLS.showFailureToast(getActivity());
			Logger.DoLog("load news detail failed!", e.getMessage(), this
					.getClass().getName(), LogType.Error);
		}
	}

}
