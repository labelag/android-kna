package com.kw.kna.pl;

import com.kw.kna.R;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import android.widget.ProgressBar;

public class CustomLoadingView extends ProgressBar {

	public CustomLoadingView(Context context) {
		super(context);
	}

	@Override
	protected void onVisibilityChanged(View changedView, int visibility) {
		super.onVisibilityChanged(changedView, visibility);
		if (visibility == View.VISIBLE)
			startAnimation(PLConfig.anim_syncer_rotate());
		else
			clearAnimation();
	}

	@Override
	protected synchronized void onDraw(Canvas canvas) {
		getContext().getResources().getDrawable(R.drawable.loading_indicator)
				.draw(canvas);
	}
}
