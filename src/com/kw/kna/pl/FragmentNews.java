package com.kw.kna.pl;

import java.io.File;
import java.util.List;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.kw.kna.MainActivity;
import com.kw.kna.R;
import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;

public class FragmentNews extends TitledListFragment implements
		OnPageChangeListener, LoaderManager.LoaderCallbacks<List<ListItemInfo>> {

	private ViewPager vFlipper;
	private final SDImageLoader mImageLoader = new SDImageLoader();
	private int currentIndex;
	private TableRow rowPager;
	private List<ListItemInfo> mTopNews;
	private ImageView[] imgPages;
	private LayoutInflater inflater;

	public android.view.View onCreateView(android.view.LayoutInflater inflater,
			android.view.ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_news, null);
		mListView = (AbsListView) v.findViewById(android.R.id.list);
		mListView.setOnItemClickListener(this);
		mListView.setAdapter(mListAdapter);
		emptyView = v.findViewById(android.R.id.empty);
		prgListLoader = v.findViewById(android.R.id.progress);
		vFlipper = (ViewPager) v.findViewById(R.id.vfSlider);
		rowPager = (TableRow) v.findViewById(R.id.rowPager);
		prgListLoader.startAnimation(PLConfig.anim_syncer_rotate());
		return v;
	}

	private void loadTopNews() {
		try {
			mTopNews = mListLoader.getTopNews();
			if (mTopNews == null || mTopNews.size() == 0) {
				getView().findViewById(R.id.layTopNews)
						.setVisibility(View.GONE);
				return;
			}
			vFlipper.removeAllViews();
			vFlipper.setOnPageChangeListener(this);
			// mSliderItemCount = picsObj.getChilds().length;
			imgPages = new ImageView[mTopNews.size()];

			TableRow.LayoutParams pagerLayout = new TableRow.LayoutParams(
					TableRow.LayoutParams.WRAP_CONTENT,
					TableRow.LayoutParams.WRAP_CONTENT);
			rowPager.removeAllViews();
			pagerLayout.setMargins(0, 0, 3, 0);
			// adImageFiles = new String[picsObj.getChilds().length];
			// mWebClient = new MyWebViewClient();
			for (int i = 0; i < mTopNews.size(); i++) {
				// adImageFiles[i] = (String) picsObj.getChilds()[i];
				imgPages[i] = new ImageView(this.getActivity());
				imgPages[i].setImageResource(R.drawable.selector_inactive);
				rowPager.addView(imgPages[i], pagerLayout);
			}
			inflater = getActivity().getLayoutInflater();
			PagerAdapter adapter = new PagerAdapter() {

				// private ListItemInfo topNewsItems;

				@Override
				public boolean isViewFromObject(View v, Object obj) {
					return v == (View) obj;
				}

				@Override
				public Object instantiateItem(ViewGroup container, int position) {
					View v = inflater.inflate(R.layout.top_news_item, null);
					ListItemInfo topNewsItem = mTopNews.get(position);
					String imgUrl = (String) topNewsItem.getExtras().get(
							R.id.itemExtra_hImage);
					// if (imgUrl == null)
					// imgUrl = topNewsItem.getImage();
					if (imgUrl != null && imgUrl.length() > 0) {
						ImageView img = (ImageView) v
								.findViewById(R.id.imgItemIcon);
						img.setTag(imgUrl);
						mImageLoader.load(
								AppConfig.ImagesFullPath
										+ AppConfig.NewsImagesDir
										+ new File(imgUrl).getName(), img,
								false);
					}

					((TextView) v.findViewById(R.id.txtItemText))
							.setText(topNewsItem.getText());
					v.setTag(position);
					v.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							ListItemInfo topNewsItem = mTopNews.get((Integer) v
									.getTag());
							((MainActivity) getActivity())
									.showNewsDetailFragment(
											topNewsItem.getId(),
											topNewsItem.getText(),
											(String) topNewsItem.getTag(),
											topNewsItem.getImage(),
											topNewsItem.getDescription());

						}
					});
					// img.setOnClickListener(getListener());
					// WebView wv = new WebView(getActivity());
					// wv.getSettings().setBuiltInZoomControls(true);
					// wv.getSettings().setLoadWithOverviewMode(true);
					// wv.setTag(adImageFiles[position]);
					// mImageLoader.load(AppConfig.ImagesFullPath + mDocId + "/"
					// + new File(adImageFiles[position]).getName(), wv,
					// false);
					// wv.loadUrl(AppConfig.ImagesFullPath + mDocId + "/"
					// + new File(adImageFiles[position]).getName());
					((ViewPager) container).addView(v);
					return v;
				}

				@Override
				public int getCount() {
					return mTopNews.size();
				}

				@Override
				public void destroyItem(ViewGroup container, int position,
						Object object) {
					// if (object instanceof ImageView)
					// ((ImageView) object).destroyDrawingCache();
					// if (container.getChildCount() > position)
					// container.removeViewAt(position);
					container.removeView((View) object);
					System.gc();
				}
			};
			vFlipper.setAdapter(adapter);
			vFlipper.setCurrentItem(0);
			currentIndex = 0;
			setFlipperPage();

		} catch (Exception e) {
			Logger.DoLog("load ad pics failed!", e.getMessage(), LogType.Error);
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int arg0) {
		setFlipperPage();
	}

	private void setFlipperPage() {
		imgPages[currentIndex].setImageResource(R.drawable.selector_inactive);
		currentIndex = vFlipper.getCurrentItem();
		imgPages[currentIndex].setImageResource(R.drawable.selector_active);
	}

	@Override
	public void onLoadFinished(Loader<List<ListItemInfo>> loader,
			List<ListItemInfo> data) {
		super.onLoadFinished(loader, data);
		loadTopNews();
		getView().findViewById(R.id.lbl_loading_news).setVisibility(View.GONE);
	}

}
