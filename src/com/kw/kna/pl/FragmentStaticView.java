package com.kw.kna.pl;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kw.kna.MainActivity;
import com.kw.kna.R;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;
import com.kw.kna.bus.enums.DataType;
import com.kw.kna.io.FileServices;
import com.kw.kna.io.JSONItemsLexicon;
import com.kw.kna.io.ParsedData;
import com.kw.kna.io.ParsedObject;

@SuppressLint("SetJavaScriptEnabled")
public class FragmentStaticView extends Fragment implements OnClickListener {

	private int mResId;
	private WebView wviewDesc;
	private GoogleMap mMap;
	private double mapLatitude;
	private double mapLongitude;

	private MapView mMapView;
	private ParsedObject docObj;
	private DataType dtype;
	private View progLoading;

	// private String mTitle;
	// private String mDesc;
	// private String mImage;
	// private String mDate;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mResId = getArguments().getInt(IntentExtraLexicon.Id);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		loadJson();
		View v;
		// if (dtype == DataType.Url) {
		// v = inflater.inflate(R.layout.frag_simple_viewer, null);
		//
		// } else {
		v = inflater.inflate(R.layout.frag_static_view, null);
		mMapView = (MapView) v.findViewById(R.id.map_view);
		mMapView.onCreate(getArguments());
		// try {
		MapsInitializer.initialize(getActivity());
		// } catch (GooglePlayServicesNotAvailableException e) {
		// Logger.DoLog("Map initializer failed!", e.getMessage(),
		// LogType.Debug);
		// }

		// }
		progLoading = v.findViewById(R.id.progUpdateData);
		wviewDesc = (WebView) v.findViewById(R.id.wviewDescription);
		wviewDesc.getSettings().setDefaultTextEncodingName("utf-8");
		wviewDesc.getSettings().setJavaScriptEnabled(true);
		wviewDesc.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		wviewDesc.getSettings().setDomStorageEnabled(true);
		wviewDesc.setWebViewClient(myWebClient);
		return v;
	}

	WebViewClient myWebClient = new WebViewClient() {
		public void onPageStarted(WebView view, String url,
				android.graphics.Bitmap favicon) {
			// if (dtype == DataType.Url)
			progLoading.setVisibility(View.VISIBLE);
			progLoading.startAnimation(PLConfig.anim_syncer_rotate());
		};

		public void onPageFinished(WebView view, String url) {
			// if (dtype == DataType.Url)
			progLoading.setVisibility(View.GONE);
			progLoading.clearAnimation();
		};

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return false;
		}
	};

	@Override
	public void onStart() {
		super.onStart();
		load();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mMapView != null)
			mMapView.onResume();
		// load();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mMapView != null)
			mMapView.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mMapView != null)
			mMapView.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		if (mMapView != null)
			mMapView.onSaveInstanceState(outState);
	}

	@Override
	public void onLowMemory() {
		// TODO Auto-generated method stub
		super.onLowMemory();
		if (mMapView != null)
			mMapView.onLowMemory();
	}

	private void loadJson() {
		try {
			ParsedData data = FileServices.readJSONFile(R.string.fj_lib);
			if (data == null || data.getObjects() == null
					|| data.getObjects().length == 0)
				throw new Exception("parse data is null or empty!");
			docObj = null;
			for (int i = 0; i < data.getObjects().length; i++) {
				if ((Integer) data.getObjects()[i].getItems().get(
						JSONItemsLexicon.id) == mResId) {
					docObj = data.getObjects()[i];
					break;
				}
			}
			if (docObj == null) {
				GeneralPLS.showToast(R.string.m_item_not_found);
				return;
			}
			dtype = DataType.fromValue(docObj
					.getStringItem(JSONItemsLexicon.type));
		} catch (Exception e) {
			GeneralPLS.showFailureToast(getActivity());
			Logger.DoLog("load static json object failed!", e.getMessage(),
					this.getClass().getName(), LogType.Error);
		}
	}

	private void load() {
		try {
			// ParsedData data = FileServices.readJSONFile(R.string.fj_lib);
			// if (data == null || data.getObjects() == null
			// || data.getObjects().length == 0)
			// throw new Exception("parse data is null or empty!");
			// docObj = null;
			// for (int i = 0; i < data.getObjects().length; i++) {
			// if ((Integer) data.getObjects()[i].getItems().get(
			// JSONItemsLexicon.id) == mResId) {
			// docObj = data.getObjects()[i];
			// break;
			// }
			// }
			// if (docObj == null) {
			// GeneralPLS.showToast(R.string.m_item_not_found);
			// return;
			// }

			if (dtype == DataType.Static)
				wviewDesc.loadUrl(docObj.getStringItem(JSONItemsLexicon.file));
			else {
				getView().setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				wviewDesc.loadUrl(docObj.getStringItem(JSONItemsLexicon.url));

			}
			// "text/html; charset=utf-8", "utf-8");
			if (Build.VERSION.SDK_INT >= 11) {
				wviewDesc.setBackgroundColor(0x01FFFFFF);
			} else {
				wviewDesc.setBackgroundColor(0x00FFFFFF);
			}

			ParsedObject objMap = (ParsedObject) docObj.getItems().get(
					JSONItemsLexicon.map);
			if (objMap == null) {
				getView().setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			} else {
				mapLatitude = Double.parseDouble(objMap
						.getStringItem(JSONItemsLexicon.latitude));
				mapLongitude = Double.parseDouble(objMap
						.getStringItem(JSONItemsLexicon.longtitude));
				int playStatus = GooglePlayServicesUtil
						.isGooglePlayServicesAvailable(getActivity());
				if (playStatus == ConnectionResult.SUCCESS) {
					// Bundle args = new Bundle();
					// args.putDouble(IntentExtraLexicon.Lat, mapLatitude);
					// args.putDouble(IntentExtraLexicon.Long, mapLongitude);
					// FragmentMapView newFragment = (FragmentMapView)
					// FragmentMapView
					// .instantiate(getActivity(),
					// FragmentMapView.class.getName(), args);
					// getChildFragmentManager().beginTransaction()
					// .add(R.id.lay_map_container, newFragment).commit();
					// Bundle args = new Bundle();
					// args.putDouble(IntentExtraLexicon.Lat, lat);
					// args.putDouble(IntentExtraLexicon.Long, longitude);
					// Fragment newFragment = FragmentMapView.instantiate(this,
					// FragmentMapView.class.getName(), args);
					// showFragment(newFragment);
					// showBackAction(true);
					// MapFragment fragMap = (MapFragment) getFragmentManager()
					// .findFragmentById(R.id.frag_gmap_view);
					new Handler().postDelayed(mapLoaderRunnable, 1000);
				} else {
					getView().findViewById(R.id.txt_show_map).setTag(
							new double[] { mapLatitude, mapLongitude });
					getView().findViewById(R.id.txt_show_map).setVisibility(
							View.VISIBLE);
					getView().findViewById(R.id.txt_show_map)
							.setOnClickListener(this);
				}
			}
		} catch (Exception e) {
			GeneralPLS.showFailureToast(getActivity());
			Logger.DoLog("load static detail failed!", e.getMessage(), this
					.getClass().getName(), LogType.Error);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.txt_show_map:
			double[] loc = (double[]) v.getTag();
			if (loc == null) {
				GeneralPLS.showFailureToast(getActivity());
				return;
			} else
				((MainActivity) getActivity()).ShowMap(loc[0], loc[1]);
			break;

		default:
			break;
		}
	}

	Runnable mapLoaderRunnable = new Runnable() {
		public void run() {
			try {
				// ((MapFragment) getFragmentManager().findFragmentById(
				// R.id.frag_map))
				getView().findViewById(R.id.img_map_divider).setVisibility(
						View.VISIBLE);
				mMapView.setVisibility(View.VISIBLE);
				mMap = mMapView.getMap();
				mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				mMap.clear();
				Marker marker = mMap.addMarker(new MarkerOptions().position(
						new LatLng(mapLatitude, mapLongitude)).flat(false));
				mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
						marker.getPosition(), 15));
			} catch (Exception e) {
				Logger.DoLog("map loader runnable failed!", e.getMessage(),
						LogType.Error);
				GeneralPLS.showFailureToast(getActivity());
			}
		}
	};
}
