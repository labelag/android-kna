package com.kw.kna.pl;

import android.os.Parcel;
import android.os.Parcelable;

public class ListConfig implements Parcelable {

	private int fileId;
	private int lastLevelFileId;
	private int parentId = -1;
	private boolean inCategoryMode;
	private String tabTitle;
	private String filePath;
	private Class<?> clickDestActivity;
	private OnListItemClickListener onClicklistener;
	private boolean thumbVisible = true;
	private String mQuery;
	private int mItemViewResId;
	private int mPageNumber;
	private boolean mIsEndless;
	private int categoryId;
	private String mSubtitle;

	public ListConfig(String tabTitle, int fileId) {
		this.tabTitle = tabTitle;
		this.fileId = fileId;
		// clickDestParams = new Bundle();
	}

	public ListConfig(String tabTitle, String filePath, int fileId) {
		this.tabTitle = tabTitle;
		this.fileId = fileId;
		this.filePath = filePath;
		// clickDestParams = new Bundle();
	}

	public String getTabTitle() {
		return tabTitle;
	}

	public int getFileId() {
		return fileId;
	}

	// public String getFilterKey() {
	// return filterKey;
	// }

	// public Object[] getFilterValues() {
	// return filterValues;
	// }

	public boolean isInCategoryMode() {
		return inCategoryMode;
	}

	public int getLastLevelFileId() {
		return lastLevelFileId;
	}

	// public void setFilterKey(String filterKey, Object[] filterValues) {
	// this.filterKey = filterKey;
	// this.filterValues = filterValues;
	// }

	public void enableCategoryMode(int fileId) {
		this.inCategoryMode = true;
		// this.lastLevelFilterKey = lastLevelFilter;
		this.lastLevelFileId = fileId;
	}

	public void disableCategoryMode() {
		this.inCategoryMode = false;
		// this.lastLevelFilterKey = null;
		this.lastLevelFileId = 0;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public Class<?> getClickDestActivity() {
		return clickDestActivity;
	}

	public void setClickDestActivity(Class<?> clickDestActivity) {
		this.clickDestActivity = clickDestActivity;
	}

	// public void addDestActivityExtra(String key, Integer val) {
	// clickDestParams.putInt(key, val);
	// }
	//
	// public void addDestActivityExtra(String key, String val) {
	// clickDestParams.putString(key, val);
	// }
	//
	// public Bundle getClickDestParams() {
	// return clickDestParams;
	// }

	public String getFilePath() {
		return filePath;
	}

	public OnListItemClickListener getOnClicklistener() {
		return onClicklistener;
	}

	public void setOnClicklistener(OnListItemClickListener onClicklistener) {
		this.onClicklistener = onClicklistener;
	}

	public void setThumbVisible(boolean thumbVisible) {
		this.thumbVisible = thumbVisible;
	}

	public boolean isThumbVisible() {
		return thumbVisible;
	}

	public void setQuery(String mQuery) {
		this.mQuery = mQuery;
	}

	public String getQuery() {
		return mQuery;
	}

	public void setItemViewResourceId(int textViewResId) {
		this.mItemViewResId = textViewResId;
	}

	public int getItemViewResId() {
		return mItemViewResId;
	}

	public void setPageNumber(int mPageNumber) {
		this.mPageNumber = mPageNumber;
	}

	public int getPageNumber() {
		return mPageNumber;
	}

	public void setEndless(boolean mIsEndless) {
		this.mIsEndless = mIsEndless;
	}

	public boolean isEndless() {
		return mIsEndless;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public void setSubtitle(String mSubtitle) {
		this.mSubtitle = mSubtitle;
	}

	public String getSubtitle() {
		return mSubtitle;
	}

	ListConfig(Parcel in) {
		fileId = in.readInt();
		lastLevelFileId = in.readInt();
		parentId = in.readInt();
		categoryId = in.readInt();
		inCategoryMode = in.readInt() == 1;
		tabTitle = in.readString();
		clickDestActivity = (Class<?>) in.readValue(Object.class
				.getClassLoader());
		thumbVisible = in.readInt() == 1;
		mQuery = in.readString();
		mItemViewResId = in.readInt();
		mPageNumber = in.readInt();
		mIsEndless = in.readInt() == 1;
		mSubtitle = in.readString();
	}

	public static final Parcelable.Creator<ListConfig> CREATOR = new Parcelable.Creator<ListConfig>() {

		public ListConfig createFromParcel(Parcel in) {
			return new ListConfig(in);
		}

		public ListConfig[] newArray(int size) {
			return new ListConfig[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel p, int arg1) {
		p.writeInt(fileId);
		p.writeInt(lastLevelFileId);
		p.writeInt(parentId);
		p.writeInt(categoryId);
		p.writeInt(inCategoryMode ? 1 : 0);
		p.writeString(tabTitle);
		p.writeValue(clickDestActivity);
		p.writeInt(thumbVisible ? 1 : 0);
		p.writeString(mQuery);
		p.writeInt(mItemViewResId);
		p.writeInt(mPageNumber);
		p.writeInt(mIsEndless ? 1 : 0);
		p.writeString(mSubtitle);
	}

}
