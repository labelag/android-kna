package com.kw.kna.pl;

public class IntentExtraLexicon {
	public static final String Id = "Id";
	public static final String CatIds = "CatIds";
	public static final String FileName = "FileName";
	public static final String Title = "Title";
	public static final String ListConfig = "ListConfig";
	public static final String Desc = "Desc";
	public static final String Date = "Date";
	public static final String Image = "Image";
	public static final String Url = "Url";
	public static final String FileId = "FileId";
	public static final String Lat = "Lat";
	public static final String Long = "Long";
}
