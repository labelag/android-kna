package com.kw.kna.pl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;

import net.sf.andpdf.nio.ByteBuffer;
import net.sf.andpdf.refs.HardReference;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.kw.kna.R;
import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFImage;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PDFPaint;

public class PDFPageLoader {

	private boolean initialized;
	private PDFFile pdfFile;
	private final float scale = 2.0f;
	private int pageCount;
	private ScaleType scaleTypeBackup;

	public PDFPageLoader(String filePath) throws Exception {
		init(filePath);
	}

	private void init(String filePath) throws Exception {
		try {
			// Settings
			PDFImage.sShowImages = true; // show images
			PDFPaint.s_doAntiAlias = true; // make text smooth
			HardReference.sKeepCaches = true; // save images in cache

			File file = new File(filePath);
			if (!file.exists()) {
				throw new FileNotFoundException("PDF file not found at "
						+ filePath);
			}
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			FileChannel channel = raf.getChannel();
			ByteBuffer bb = ByteBuffer.NEW(channel.map(
					FileChannel.MapMode.READ_ONLY, 0, channel.size()));
			raf.close();
			// create a pdf doc
			pdfFile = new PDFFile(bb);

			this.initialized = true;
			this.pageCount = pdfFile.getNumPages();
		} catch (Exception e) {
			Logger.DoLog("init pdf loader failed!", e.getMessage(),
					LogType.Error);
			throw new Exception("Init pdf page loader failed! "
					+ e.getMessage());
		}
	}

	public void load(int pageIndex, ImageView v) {
		if (!initialized)
			throw new IllegalStateException("PDF loader not initialized!");
		if (pageIndex < 0 || pageIndex >= pageCount)
			throw new IndexOutOfBoundsException("Page index is not valid!");
		if (cancelPotentialSDLoad(pageIndex, v)) {
			SDLoadImageTask task = new SDLoadImageTask(v);
			v.setTag(R.id.tag_SDLoadTask, new WeakReference<SDLoadImageTask>(
					task));
			// if (FileServices.checkFileExist(pageIndex)) {
			// // SDLoadDrawable sdDrawable = new SDLoadDrawable(task);
			// v.setImageDrawable(new ColorDrawable(Color.rgb(255, 255, 255)));
			// task.execute(pageIndex);
			// } else {
			v.setImageResource(R.drawable.loading_indicator);
			v.startAnimation(PLConfig.anim_syncer_rotate());
			scaleTypeBackup = v.getScaleType();
			v.setScaleType(ScaleType.CENTER);
			// task.setToBeDownload(true);
			task.executeOnParallel(pageIndex);
			// }
		}
	}

	public int getPageCount() {
		return pageCount;
	}

	public boolean isInitialized() {
		return initialized;
	}

	private Bitmap loadImageFromSDCard(int pageIndex) {
		Bitmap photo = null;
		try {
			if (!initialized) {
				return BitmapFactory.decodeResource(
						AppConfig.Application.getResources(),
						android.R.drawable.ic_menu_report_image);
			}
			PDFPage page = pdfFile.getPage(pageIndex + 1, true);
			photo = page.getImage((int) (page.getWidth() * scale),
					(int) (page.getHeight() * scale), null, true, true);
		} catch (Exception e) {
			Logger.DoLog("load image form SD card failed!", e.getMessage(),
					LogType.Error);
		}
		return photo;

	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					|| (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
		try {
			Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
					bitmap.getHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(output);

			final int color = 0xff424242;
			final Paint paint = new Paint();
			final Rect rect = new Rect(0, 0, bitmap.getWidth(),
					bitmap.getHeight());
			final RectF rectF = new RectF(rect);
			final float roundPx = pixels;

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			canvas.drawBitmap(bitmap, rect, rect, paint);

			return output;

		} catch (Exception e) {
			Logger.DoLog("get rounded corner bitmap failed!", e.getMessage(),
					LogType.Error);
			return bitmap;
		}
	}

	private static boolean cancelPotentialSDLoad(int pageIndex, ImageView v) {
		SDLoadImageTask sdLoadTask = getAsyncSDLoadImageTask(v);
		if (sdLoadTask != null) {
			int taskPageIndex = sdLoadTask.getPageIndex();
			if (taskPageIndex != pageIndex) {
				sdLoadTask.cancel(true);
			} else {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private static SDLoadImageTask getAsyncSDLoadImageTask(ImageView v) {
		if (v != null) {
			Object tag = v.getTag(R.id.tag_SDLoadTask);
			// Drawable drawable = v.getDrawable();
			if (tag != null && tag instanceof WeakReference<?>) {
				// SDLoadDrawable asyncLoadedDrawable = (SDLoadDrawable)
				// drawable;
				// return asyncLoadedDrawable.getAsyncSDLoadTask();
				return ((WeakReference<SDLoadImageTask>) tag).get();
			}
		}
		return null;
	}

	private class SDLoadImageTask extends AsyncTask<Integer, Void, Bitmap> {

		private int mPageIndex;
		private final WeakReference<ImageView> mImageViewReference;

		// private ProgressDialog progDialog;

		// private boolean mForListView;
		// private boolean mToBeDownload;
		// private String mImageUrl;

		public int getPageIndex() {
			return mPageIndex;
		}

		// public void setToBeDownload(boolean b) {
		// this.mToBeDownload = b;
		//
		// }

		public SDLoadImageTask(ImageView v) {
			mImageViewReference = new WeakReference<ImageView>(v);
			// mImageUrl = (String) v.getTag();
			// mForListView = forList;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// progDialog = ProgressDialog.show(context, null, null, true,
			// false);
		}

		@Override
		protected void onPostExecute(Bitmap bmp) {
			// if (progDialog != null)
			// progDialog.dismiss();
			if (mImageViewReference != null && bmp != null) {
				ImageView v = mImageViewReference.get();
				SDLoadImageTask sdLoadTask = getAsyncSDLoadImageTask(v);
				// Change bitmap only if this process is still associated with
				// it
				if (this == sdLoadTask && v != null) {
					v.clearAnimation();
					v.setImageBitmap(bmp);
					v.setScaleType(scaleTypeBackup);
				}
				bmp = null;
			}
		}

		@Override
		protected Bitmap doInBackground(Integer... params) {
			mPageIndex = params[0];
			// if (mToBeDownload && mImageUrl != null) {
			// // boolean updateRes =
			// new FileServices().updateDocImage(mPageIndex, mImageUrl);
			// // if (!updateRes) {
			// // return BitmapFactory.decodeResource(
			// // AppConfig.Application.getResources(),
			// // android.R.drawable.ic_menu_report_image);
			// // }
			// }
			return loadImageFromSDCard(mPageIndex);
		}

		public void executeOnParallel(Integer... params) {

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				try {
					this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
							params);
				} catch (Exception e) {
					Logger.DoLog("execute on parallel failed!", this.getClass()
							.getName(), LogType.Error);
					// if (this.getStatus() != Status.RUNNING)
					// this.execute(params);
				}
			} else {
				this.execute(params);
			}

		}
	}

}