package com.kw.kna;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.kw.kna.pl.FragmentDocView;
import com.kw.kna.pl.FragmentNewsDetail;
import com.kw.kna.pl.FragmentStaticView;
import com.kw.kna.pl.GeneralPLS;
import com.kw.kna.pl.IntentExtraLexicon;

public class MainActivity extends MasterActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_main);
		super.onCreate(savedInstanceState);
		showNewsFragment();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		default:
			super.onClick(v);
			break;
		}

	}

	public FragmentDocView showDocViewFragment(int docId, int fileId) {
		Bundle args = new Bundle();
		args.putInt(IntentExtraLexicon.Id, docId);
		args.putInt(IntentExtraLexicon.FileId, fileId);
		FragmentDocView newFragment = (FragmentDocView) FragmentDocView
				.instantiate(this, FragmentDocView.class.getName(), args);
		showFragment(newFragment);
		showBackAction(true);
		return newFragment;
	}

	public void showPDFViewerFragment(String filePath, String url) {

		Intent intent = new Intent(this, PDFActivity.class);
		intent.setAction(Intent.ACTION_VIEW);
		intent.setData(Uri.parse(filePath));
		intent.putExtra(IntentExtraLexicon.FileName, filePath);
		intent.putExtra(IntentExtraLexicon.Url, url);
		startActivity(intent);

		// Bundle args = new Bundle();
		// args.putString(IntentExtraLexicon.FileName, filePath);
		// args.putString(IntentExtraLexicon.Url, url);
		// Fragment newFragment = FragmentPDFView.instantiate(this,
		// FragmentPDFView.class.getName(), args);
		// showFragment(newFragment);
		// showBackAction(true);
	}

	public void showNewsDetailFragment(int itemId, String title, String desc,
			String image, String date) {
		Bundle args = new Bundle();
		args.putInt(IntentExtraLexicon.Id, itemId);
		args.putString(IntentExtraLexicon.Title, title);
		args.putString(IntentExtraLexicon.Desc, desc);
		args.putString(IntentExtraLexicon.Image, image);
		args.putString(IntentExtraLexicon.Date, date);
		Fragment newFragment = FragmentNewsDetail.instantiate(this,
				FragmentNewsDetail.class.getName(), args);
		showFragment(newFragment);
		showBackAction(true);
		// return newFragment;
	}

	public void showStaticViewFragment(int docId) {
		Bundle args = new Bundle();
		args.putInt(IntentExtraLexicon.Id, docId);
		Fragment newFragment = FragmentStaticView.instantiate(this,
				FragmentStaticView.class.getName(), args);
		showFragment(newFragment);
		showBackAction(true);
	}

	public void ShowMap(double lat, double longitude) {
		try {
			String query = lat + "," + longitude;
			String uriBegin = "geo:" + query;
			String uriString = uriBegin + "?q=" + Uri.encode(query) + "&z=17";
			Uri uri = Uri.parse(uriString);
			Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
			this.startActivity(intent);
		} catch (Exception e) {
			GeneralPLS.showToast(R.string.m_map_is_not_available);
		}
	}
}
