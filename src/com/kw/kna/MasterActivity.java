package com.kw.kna;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.kw.kna.pl.FragmentDocView;
import com.kw.kna.pl.FragmentNews;
import com.kw.kna.pl.FragmentNewsDetail;
import com.kw.kna.pl.FragmentPDFView;
import com.kw.kna.pl.IntentExtraLexicon;
import com.kw.kna.pl.ListConfig;
import com.kw.kna.pl.ListFragment;
import com.kw.kna.pl.ListItemInfo;
import com.kw.kna.pl.OnListItemClickListener;
import com.kw.kna.pl.TitledListFragment;

public class MasterActivity extends FragmentActivity implements
		OnClickListener, OnListItemClickListener {

	protected boolean mInSearchMode;
	protected TitledListFragment msearchFragment;
	protected Fragment mCurrentFragment;
	protected ArrayList<Fragment> mFragments;
	protected int mCurrentFragmentIndex;
	private int mdrawerGravity = Gravity.RIGHT;
	private DrawerLayout mDrawerLayout;
	private ArrayList<TitledListFragment> mSideMenuFragments;
	private int currentFileId;
	private ActionBarDrawerToggle mDrawerToggle;
	private View side_drawer;
	private View frame;
	private float lastTranslate = 0.0f;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setBackgroundDrawable(
				getResources().getDrawable(R.drawable.actionbar_bg));
		getActionBar().setIcon(null);
		getActionBar().setDisplayShowHomeEnabled(false);
		showBackAction(false);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mFragments = new ArrayList<Fragment>();
		initSideMenu();
	}

	public void showBackAction(boolean show) {
		if (show) {
			getActionBar().setTitle(R.string.back);
			getActionBar().setDisplayHomeAsUpEnabled(true);
		} else {
			getActionBar().setTitle(null);
			getActionBar().setDisplayHomeAsUpEnabled(false);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			if (mDrawerLayout.isDrawerOpen(mdrawerGravity))
				mDrawerLayout.closeDrawer(mdrawerGravity);
			else
				mDrawerLayout.openDrawer(mdrawerGravity);
			return true;
		default:
			return super.onOptionsItemSelected(item);

		}

	}

	protected ListFragment showListFragment(ListConfig lstConfig) {
		if (mCurrentFragment instanceof ListFragment
				&& ((ListFragment) mCurrentFragment).getFileId() == lstConfig
						.getFileId()) {
			// if current fragment is cities fragment , do nothing!
			return (ListFragment) mCurrentFragment;
		} else {
			Bundle args = new Bundle();
			args.putParcelable(IntentExtraLexicon.ListConfig, lstConfig);
			ListFragment newFragment = (ListFragment) ListFragment.instantiate(
					this, ListFragment.class.getName(), args);
			showFragment(newFragment);
			return newFragment;
		}
	}

	public TitledListFragment showAutoUpdateFragment(ListConfig lstConfig) {
		return showAutoUpdateFragment(lstConfig, false);
	}

	public TitledListFragment showAutoUpdateFragment(ListConfig lstConfig,
			boolean clearTop) {
		if (mCurrentFragment instanceof TitledListFragment
				&& ((TitledListFragment) mCurrentFragment).getFileId() == lstConfig
						.getFileId()) {
			// if current fragment is cities fragment , do nothing!
			return (TitledListFragment) mCurrentFragment;
		} else {
			Bundle args = new Bundle();
			args.putParcelable(IntentExtraLexicon.ListConfig, lstConfig);
			TitledListFragment newFragment = (TitledListFragment) TitledListFragment
					.instantiate(this, TitledListFragment.class.getName(), args);
			showFragment(newFragment, clearTop);
			return newFragment;
		}
	}

	protected void showFragment(Fragment frag) {
		showFragment(frag, false);
	}

	protected void showFragment(Fragment frag, boolean clearTop) {
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
				R.animator.fragment_slide_right_exit);
		ft.replace(R.id.fragment_container, frag);
		ft.addToBackStack(null);
		if (mCurrentFragment != null)
			ft.remove(mCurrentFragment);
		ft.commit();
		if (clearTop) {
			mFragments.clear();
			showBackAction(false);
		}
		mFragments.add(frag);
		mCurrentFragment = frag;
		mCurrentFragmentIndex = mFragments.size() - 1;
		// tblHomeList.setVisibility(View.GONE);
	}

	public void removeFromFragmentStack() {
		Fragment toRemove = null;
		if (mFragments.size() > 0) {
			// if (mCurrentFragment instanceof TitledListFragment
			// && ((TitledListFragment) mCurrentFragment).handleBack()) {
			// return;
			// }
			toRemove = mFragments.remove(mCurrentFragmentIndex);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			if (toRemove != null)
				ft.remove(toRemove);
			ft.commit();
		}
		if (mFragments.size() > 0) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.animator.fragment_slide_right_enter,
					R.animator.fragment_slide_left_exit);
			mCurrentFragmentIndex = mFragments.size() - 1;
			mCurrentFragment = mFragments.get(mCurrentFragmentIndex);
			ft.replace(R.id.fragment_container, mCurrentFragment);

			ft.commit();
			if (mCurrentFragment instanceof FragmentNewsDetail) {
				setTabMenuSelected(R.string.fj_news_detail);
			} else if (mCurrentFragment instanceof FragmentPDFView) {
				setTabMenuSelected(R.string.fj_member_items);
			} else if (mCurrentFragment instanceof ListFragment) {
				setTabMenuSelected(((ListFragment) mCurrentFragment)
						.getFileId());
			} else if (mCurrentFragment instanceof FragmentDocView) {
				setTabMenuSelected(((FragmentDocView) mCurrentFragment)
						.getFileId());
			}

		}
		// else
		// showMainList();
	}

	public boolean backwardFragmentStack() {
		if (mFragments.size() > mCurrentFragmentIndex
				&& mCurrentFragmentIndex > 0) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.animator.fragment_slide_right_enter,
					R.animator.fragment_slide_left_exit);
			ft.remove(mCurrentFragment);
			mCurrentFragmentIndex--;
			mCurrentFragment = mFragments.get(mCurrentFragmentIndex);
			ft.replace(R.id.fragment_container, mCurrentFragment);
			ft.addToBackStack(null);

			ft.commit();
			// tblHomeList.setVisibility(View.GONE);
			return true;
			// } else {
			// showMainList();
		}
		return false;
	}

	public boolean forwardFragmentStack() {
		if (mFragments.size() > 0
				&& mFragments.size() > mCurrentFragmentIndex + 1) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setCustomAnimations(R.animator.fragment_slide_left_enter,
					R.animator.fragment_slide_right_exit);
			mCurrentFragmentIndex++;
			mCurrentFragment = mFragments.get(mCurrentFragmentIndex);
			ft.replace(R.id.fragment_container, mCurrentFragment);
			ft.addToBackStack(null);
			ft.commit();
			// tblHomeList.setVisibility(View.GONE);
			return true;
		}
		return false;
	}

	public void showNewsFragment() {
		currentFileId = R.string.fj_news;
		// setTabMenuSelected(currentFileId);
		ListConfig lstConfig = new ListConfig(null, currentFileId);
		lstConfig.setItemViewResourceId(R.layout.news_list_item);
		if (mCurrentFragment instanceof FragmentNews) {
			// if current fragment is news fragment , do nothing!
			// return (FragmentNews) mCurrentFragment;

		} else {
			Bundle args = new Bundle();
			args.putParcelable(IntentExtraLexicon.ListConfig, lstConfig);
			FragmentNews newFragment = (FragmentNews) FragmentNews.instantiate(
					this, FragmentNews.class.getName(), args);
			showFragment(newFragment, true);
			// return newFragment;
		}

	}

	@Override
	public void onBackPressed() {
		if (mFragments.size() <= 1)
			this.finish();
		else {
			removeFromFragmentStack();
			if (mFragments.size() <= 1)
				showBackAction(false);

		}
	}

	public void setTabMenuSelected(int fileId) {
		// Logger.DoLog("set tab icon", fileId + "", LogType.Debug);
		ViewGroup layTabbar = (ViewGroup) findViewById(R.id.layTabBar);
		for (int i = 0; i < layTabbar.getChildCount(); i++) {
			layTabbar.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
		}
		mDrawerLayout.closeDrawer(mdrawerGravity);
		switch (fileId) {
		case R.string.fj_lib:
		case R.string.fj_doc_detail:
			findViewById(R.id.imbtn_lib).setBackgroundResource(
					R.drawable.ic_tab_lib_selected_bg);
			break;
		case R.string.fj_gallery_list:
		case R.string.fj_gallety_items:
			findViewById(R.id.imbtn_media).setBackgroundResource(
					R.drawable.ic_tab_media_selected_bg);
			break;
		case R.string.fj_members_list:
		case R.string.fj_member_items:
			findViewById(R.id.imbtn_members).setBackgroundResource(
					R.drawable.ic_tab_members_selected_bg);
			break;
		case R.string.fj_news:
		case R.string.fj_news_detail:
			findViewById(R.id.imbtn_news).setBackgroundResource(
					R.drawable.ic_tab_news_selected_bg);
			break;
		case R.string.fj_about:
			findViewById(R.id.imbtn_about).setBackgroundResource(
					R.drawable.ic_tab_about_selected_bg);
			break;
		default:
			break;
		}
	}

	private void initSideMenu() {
		side_drawer = findViewById(R.id.drawer_main_right);
		frame = findViewById(R.id.frame_content);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_menu_top, 0, 0) {
			@SuppressLint("NewApi")
			public void onDrawerSlide(View drawerView, float slideOffset) {
				float moveFactor = (side_drawer.getWidth() * slideOffset) * -1;

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					frame.setTranslationX(moveFactor);
				} else {
					TranslateAnimation anim = new TranslateAnimation(
							lastTranslate, moveFactor, 0.0f, 0.0f);
					anim.setDuration(0);
					anim.setFillAfter(true);
					frame.startAnimation(anim);
					lastTranslate = moveFactor;
				}
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		mSideMenuFragments = new ArrayList<TitledListFragment>();
		mSideMenuFragments.add(showSideMenuFragment(R.string.fj_lib,
				R.layout.side_menu_list_item, R.id.container_menu_lib));
		mSideMenuFragments.add(showSideMenuFragment(R.string.fj_members_list,
				R.layout.side_menu_list_item, R.id.container_menu_members));
		mSideMenuFragments.add(showSideMenuFragment(R.string.fj_about,
				R.layout.side_menu_list_item, R.id.container_menu_about));
		mSideMenuFragments
				.add(showSideMenuFragment(R.string.fj_constitution,
						R.layout.side_menu_list_item,
						R.id.container_menu_constitution));
		mSideMenuFragments.add(showSideMenuFragment(R.string.fj_memo,
				R.layout.side_menu_list_item, R.id.container_menu_memo));
		((EditText) findViewById(R.id.etxtSearch))
				.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
						if (mSideMenuFragments != null) {
							TitledListFragment tmpList;
							for (int i = 0; i < mSideMenuFragments.size(); i++) {
								tmpList = ((TitledListFragment) mSideMenuFragments
										.get(i));
								tmpList.onQueryTextChange(s.toString());
							}
							new Handler().postDelayed(afterSearchRunnable, 500);
							new Handler()
									.postDelayed(afterSearchRunnable, 2000);
						}
					}
				});
		new Handler().postDelayed(sideListsRunnable, 4000);
	}

	Runnable afterSearchRunnable = new Runnable() {
		public void run() {
			if (mSideMenuFragments != null) {
				TitledListFragment tmpList;
				for (int i = 0; i < mSideMenuFragments.size(); i++) {
					tmpList = ((TitledListFragment) mSideMenuFragments.get(i));
					updateListViewHeight((ListView) tmpList.getListView());
					int vis = View.GONE;
					if (((ListView) tmpList.getListView()).getCount() > 0
							&& ((EditText) findViewById(R.id.etxtSearch))
									.getText().length() > 0)
						vis = View.VISIBLE;
					switch (tmpList.getFileId()) {
					case R.string.fj_about:
						findViewById(R.id.container_menu_about).setVisibility(
								vis);
						break;
					case R.string.fj_lib:
						findViewById(R.id.container_menu_lib)
								.setVisibility(vis);
						break;
					case R.string.fj_members_list:
						findViewById(R.id.container_menu_members)
								.setVisibility(vis);
						break;
					case R.string.fj_constitution:
						findViewById(R.id.container_menu_constitution)
								.setVisibility(vis);
						break;
					case R.string.fj_memo:
						findViewById(R.id.container_menu_memo).setVisibility(
								vis);
						break;
					}
				}

			}
		}
	};

	protected TitledListFragment showSideMenuFragment(int currentFileId,
			int itemResource, int containerId) {
		ListConfig lstConfig = new ListConfig(null, currentFileId);
		lstConfig.setItemViewResourceId(itemResource);
		lstConfig.setOnClicklistener(this);
		Bundle args = new Bundle();
		args.putParcelable(IntentExtraLexicon.ListConfig, lstConfig);
		TitledListFragment newFragment = (TitledListFragment) TitledListFragment
				.instantiate(this, TitledListFragment.class.getName(), args);
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.replace(containerId, newFragment);
		ft.commit();
		return newFragment;
	}

	Runnable sideListsRunnable = new Runnable() {
		public void run() {
			if (mSideMenuFragments != null) {
				for (int i = 0; i < mSideMenuFragments.size(); i++) {
					updateListViewHeight((ListView) mSideMenuFragments.get(i)
							.getListView());
				}
			}
		}
	};

	/****
	 * Method for Setting the Height of the ListView dynamically. Hack to fix
	 * the issue of not showing all the items of the ListView when placed inside
	 * a ScrollView
	 ****/
	public static void updateListViewHeight(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imbtn_about:
		case R.id.btn_menu_about:
			currentFileId = R.string.fj_about;
			showListDataFragment(currentFileId, R.layout.lib_list_item);
			break;
		case R.id.imbtn_lib:
		case R.id.btn_menu_lib:
			currentFileId = R.string.fj_lib;
			showListDataFragment(currentFileId, R.layout.lib_list_item);
			break;
		case R.id.imbtn_media:
		case R.id.btn_menu_gallery:
			currentFileId = R.string.fj_gallery_list;
			showListDataFragment(currentFileId, R.layout.album_list_item);
			break;
		case R.id.imbtn_members:
		case R.id.btn_menu_members:
			currentFileId = R.string.fj_members_list;
			showListDataFragment(currentFileId, R.layout.lib_list_item);
			break;
		case R.id.imbtn_news:
		case R.id.btn_menu_news:
			showNewsFragment();
			break;
		case R.id.img_menu_lib_expand:
			View cv = findViewById(R.id.container_menu_lib);
			if (cv.getVisibility() == View.VISIBLE)
				cv.setVisibility(View.GONE);
			else
				cv.setVisibility(View.VISIBLE);
			break;
		case R.id.img_menu_members_expand:
			cv = findViewById(R.id.container_menu_members);
			if (cv.getVisibility() == View.VISIBLE)
				cv.setVisibility(View.GONE);
			else
				cv.setVisibility(View.VISIBLE);
			break;
		case R.id.img_menu_about_expand:
			cv = findViewById(R.id.container_menu_about);
			if (cv.getVisibility() == View.VISIBLE)
				cv.setVisibility(View.GONE);
			else
				cv.setVisibility(View.VISIBLE);
			break;
		case R.id.img_menu_constitution_expand:
		case R.id.btn_menu_constitution:
			cv = findViewById(R.id.container_menu_constitution);
			if (cv.getVisibility() == View.VISIBLE)
				cv.setVisibility(View.GONE);
			else
				cv.setVisibility(View.VISIBLE);
			break;
		case R.id.img_menu_memo_expand:
		case R.id.btn_menu_memo:
			cv = findViewById(R.id.container_menu_memo);
			if (cv.getVisibility() == View.VISIBLE)
				cv.setVisibility(View.GONE);
			else
				cv.setVisibility(View.VISIBLE);
			break;
		case R.id.imbtn_search:
			break;
		default:
			break;
		}
	}

	private void showListDataFragment(int fileId, int itemResource) {
		ListConfig lstConf = new ListConfig(null, fileId);
		lstConf.setItemViewResourceId(itemResource);
		showAutoUpdateFragment(lstConf, true);
		// setTabMenuSelected(fileId);
	}

	@Override
	public void onListItemClick(ListItemInfo itemInfo, int index) {
		mDrawerLayout.closeDrawer(mdrawerGravity);
	}
}
