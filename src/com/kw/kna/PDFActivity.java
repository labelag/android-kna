package com.kw.kna;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.artifex.mupdfdemo.MuPDFActivity;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;
import com.kw.kna.bus.enums.AsyncOperationResult;
import com.kw.kna.bus.enums.AsyncOperationType;
import com.kw.kna.io.AsyncOperationTask;
import com.kw.kna.io.FileServices;
import com.kw.kna.io.OnAsyncOperationListener;
import com.kw.kna.pl.GeneralPLS;
import com.kw.kna.pl.IntentExtraLexicon;
import com.kw.kna.pl.PLConfig;

public class PDFActivity extends MuPDFActivity implements
		OnAsyncOperationListener {

	// protected boolean mInSearchMode;
	// protected TitledListFragment msearchFragment;
	// protected Fragment mCurrentFragment;
	// protected ArrayList<Fragment> mFragments;
	// protected int mCurrentFragmentIndex;
	// private int mdrawerGravity = Gravity.RIGHT;
	// private DrawerLayout mDrawerLayout;
	// private ArrayList<TitledListFragment> mSideMenuFragments;
	// private int currentFileId;
	// private ActionBarDrawerToggle mDrawerToggle;
	// private View side_drawer;
	// private View frame;
	// private float lastTranslate = 0.0f;
	private ProgressBar progLoading;
	private View vProgressLayout;
	private TextView txtProgress;
	private String mFilePath;
	private String mFileUrl;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setBackgroundDrawable(
				getResources().getDrawable(R.drawable.actionbar_bg));
		getActionBar().setIcon(null);
		getActionBar().setDisplayShowHomeEnabled(false);
		showBackAction(true);
		mFilePath = getIntent().getStringExtra(IntentExtraLexicon.FileName);
		mFileUrl = getIntent().getStringExtra(IntentExtraLexicon.Url);
		if (!FileServices.checkFileExist(mFilePath)) {
			ViewGroup v = (ViewGroup) this.findViewById(android.R.id.content);
			vProgressLayout = getLayoutInflater().inflate(
					R.layout.progress_layout, null);
			progLoading = (ProgressBar) vProgressLayout
					.findViewById(R.id.progUpdateData);
			txtProgress = (TextView) vProgressLayout
					.findViewById(R.id.txt_progress);
			v.addView(vProgressLayout);
			progLoading.startAnimation(PLConfig.anim_syncer_rotate());
			AsyncOperationTask asyncTask = new AsyncOperationTask(this,
					AsyncOperationType.GetPDF);
			asyncTask.executeOnParallel(new Object[] { mFilePath, mFileUrl });
		}
	}

	public void showBackAction(boolean show) {
		if (show) {
			getActionBar().setTitle(R.string.back);
			getActionBar().setDisplayHomeAsUpEnabled(true);
		} else {
			getActionBar().setTitle(null);
			getActionBar().setDisplayHomeAsUpEnabled(false);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// case R.id.action_settings:
		// if (mDrawerLayout.isDrawerOpen(mdrawerGravity))
		// mDrawerLayout.closeDrawer(mdrawerGravity);
		// else
		// mDrawerLayout.openDrawer(mdrawerGravity);
		// return true;
		case android.R.id.home:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);

		}

	}

	@Override
	public void onBackPressed() {
		this.finish();
	}

	@Override
	public void onDone(AsyncOperationType type, AsyncOperationResult result,
			Object response) {
		try {
			switch (type) {
			case GetPDF:
				vProgressLayout.setVisibility(View.GONE);
				progLoading.clearAnimation();
				GeneralPLS.handleGeneralAsyncOperationResult(result, this,
						false);
				if (result == AsyncOperationResult.Success) {
					super.onCreate(null);
				}
				break;

			default:
				break;
			}
		} catch (Exception e) {
			Logger.DoLog("handle get pdf task done failed!", e.toString(),
					LogType.Error);
			// GeneralPLS.showFailureToast(this);
		}
	}

	@Override
	public void onProgress(AsyncOperationType type, int progress) {
		switch (type) {
		case GetPDF:
			txtProgress.setText(progress + "%");
			break;
		default:
			break;
		}

	}

}
