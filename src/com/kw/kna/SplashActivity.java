package com.kw.kna;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.kw.kna.bus.AppConfig;

public class SplashActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		new Handler().postDelayed(splashRunnable, AppConfig.SplashDuration);
	}

	Runnable splashRunnable = new Runnable() {
		public void run() {
			Intent intent = new Intent(getContext(), MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
//			overridePendingTransition(R.animator.fragment_slide_left_enter,
//					R.animator.fragment_slide_right_exit);
			getContext().finish();
		}
	};

	private Activity getContext() {
		return this;
	}
}
