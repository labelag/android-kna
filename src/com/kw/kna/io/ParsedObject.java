package com.kw.kna.io;

import java.util.Hashtable;

import org.json.JSONObject;

public class ParsedObject {
	private Hashtable<String, Object> items;
	private Object[] childs;
	private boolean isArray;

	public Hashtable<String, Object> getItems() {
		return items;
	}

	public Object[] getChilds() {
		return childs;
	}

	public ParsedObject(Hashtable<String, Object> items) {
		this.items = items;
		if (this.items == null)
			this.items = new Hashtable<String, Object>();
	}

	public ParsedObject(Object[] childs) {
		this.childs = childs;
		this.isArray = true;
	}

	public boolean isArray() {
		return isArray;
	}

	public String getStringItem(String key) {
		if (getItems().get(key) == JSONObject.NULL)
			return null;
		return (String) getItems().get(key);
	}

}
