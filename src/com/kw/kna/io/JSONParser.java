/**
 * 
 */
package com.kw.kna.io;

import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;

/**
 * @author Mohamad
 * 
 */
public class JSONParser {

	// private static String _ClassScope = "json.JSONParser";
	// private static String _ClassCode = "300";

	public static ParsedData parseJSON(String content, String[] keys) {
		return parseJSON(content, 0);
	}

	public static ParsedData parseJSON(String content, int fileId) {
		// String _mScope = _ClassScope + "parseJSON";
		try {
			ParsedObject[] objects = null;
			boolean success = false;
			// JSONObject jobj;
			switch (fileId) {
			default:
				success = true;
				JSONArray jar = new JSONArray(content);
				objects = readParsedJSONArray(jar);
				break;
			}
			return new ParsedData(success, objects);
		} catch (Exception e) {
			Logger.DoLog("parse json file failed!", e.getMessage(),
					JSONParser.class.getName(), LogType.Error);
			e.printStackTrace();
			return null;
		}
	}

	private static Object[] readJSONArray(JSONArray jar) throws JSONException {
		Object[] result = new Object[jar.length()];
		try {
			Object obj;
			for (int i = 0; i < result.length; i++) {
				obj = jar.get(i);
				if (obj instanceof JSONArray) {
					result[i] = new ParsedObject(readJSONArray((JSONArray) obj));
				} else if (obj instanceof JSONObject) {
					result[i] = readJSONObject((JSONObject) obj);
				} else {
					result[i] = obj;
				}
			}
			return result;
		} catch (JSONException e) {
			Logger.DoLog("read JSON array failed!", e.getMessage(),
					LogType.Error);
			throw e;
		}
	}

	private static ParsedObject[] readParsedJSONArray(JSONArray jar)
			throws JSONException {
		ParsedObject[] result = new ParsedObject[jar.length()];
		try {
			Object obj;
			for (int i = 0; i < result.length; i++) {
				obj = jar.get(i);
				if (obj instanceof JSONArray) {
					result[i] = new ParsedObject(readJSONArray((JSONArray) obj));
				} else if (obj instanceof JSONObject) {
					result[i] = readJSONObject((JSONObject) obj);
				} else {
					result[i] = null;
				}
			}
			return result;
		} catch (JSONException e) {
			Logger.DoLog("read JSON array failed!", e.getMessage(),
					LogType.Error);
			throw e;
		}
	}

	private static ParsedObject readJSONObject(JSONObject jobj)
			throws JSONException {
		Hashtable<String, Object> items = new Hashtable<String, Object>();
		Object obj;
		String key;
		try {

			for (int i = 0; i < jobj.names().length(); i++) {
				key = (String) jobj.names().getString(i);
				obj = jobj.get(key);
				// }
				// while (jobj.keys().hasNext()) {
				// key = (String) jobj.keys().next();
				// obj = jobj.get(key);
				if (obj instanceof JSONArray) {
					items.put(key, new ParsedObject(
							readJSONArray((JSONArray) obj)));
				} else if (obj instanceof JSONObject) {
					items.put(key, readJSONObject((JSONObject) obj));
				} else
					items.put(key, obj);
			}
			return new ParsedObject(items);
		} catch (JSONException e) {
			Logger.DoLog("read JSON object failed!", e.getMessage(),
					LogType.Error);
			throw e;
		}
	}
}
