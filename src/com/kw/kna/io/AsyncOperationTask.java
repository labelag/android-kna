/**
 * 
 */
package com.kw.kna.io;

import android.os.AsyncTask;
import android.os.Build;

import com.kw.kna.R;
import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;
import com.kw.kna.bus.enums.AsyncOperationResult;
import com.kw.kna.bus.enums.AsyncOperationType;
import com.kw.kna.bus.enums.WebQueryService;

public class AsyncOperationTask extends
		AsyncTask<Object, Integer, AsyncOperationResult> {

	private static final String RSSEndTag = "</rss>";
	private OnAsyncOperationListener mListener;
	private AsyncOperationType mType;
	private Object responseData;

	public AsyncOperationTask(OnAsyncOperationListener listener,
			AsyncOperationType type) {
		mListener = listener;
		mType = type;
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}

	public void executeOnParallel(Object... params) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
		} else {
			this.execute(params);
		}
	}

	@Override
	protected AsyncOperationResult doInBackground(Object... arg0) {
		try {
			if (isCancelled())
				return AsyncOperationResult.Cancel;
			return doOperation(arg0);
		} catch (Exception e) {
			Logger.DoLog("Do async operation failed!",
					"OperationType:" + mType.name() + ";" + e.getMessage(),
					LogType.Error);
			return AsyncOperationResult.Failure;
		}
	}

	@Override
	protected void onPostExecute(AsyncOperationResult result) {
		if (mListener != null)
			mListener.onDone(mType, result, responseData);
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		if (mListener != null)
			mListener.onProgress(mType, values[0]);
	}

	private AsyncOperationResult doOperation(Object... arg0) {
		return doOperationOnThread(arg0);
	}

	public AsyncOperationResult doOperationOnThread(Object... args) {
		AsyncOperationResult result = AsyncOperationResult.Failure;
		try {
			switch (mType) {
			case UpdateDocuments:
				// check network connectivity
				// if (!NetworkServices.isNetworkAvailable())
				// return AsyncOperationResult.NetworkNotAvailable;
				result = updateDataFile(R.string.fj_lib, args);
				break;
			case UpdateFileByUrl:
				// check network connectivity
				// if (!NetworkServices.isNetworkAvailable())
				// return AsyncOperationResult.NetworkNotAvailable;
				if (new FileServices().updateDocImage((String) args[0],
						(String) args[1]))
					result = AsyncOperationResult.Success;
				break;
			case GetPDF:
				// check network connectivity
				if (!NetworkServices.isNetworkAvailable())
					return AsyncOperationResult.NetworkNotAvailable;
				// Logger.DoLog("dl pdf : "+args[0],LogType.Debug);
				if (new FileServices().updateDocImage((String) args[0],
						(String) args[1], this)) {
					result = AsyncOperationResult.Success;
					// responseData = extractPDF((String) arg0[0]);
				}
				break;
			case UpdateNewsList:
				// check network connectivity
				// if (!NetworkServices.isNetworkAvailable())
				// return AsyncOperationResult.NetworkNotAvailable;
				result = updateDataFile(R.string.fj_news, args);
				break;
			default:
				break;
			}
		} catch (Exception e) {
			Logger.DoLog("Do async operation failed!",
					"OperationType:" + mType.name() + ";" + e.getMessage(),
					LogType.Error);
			result = AsyncOperationResult.Failure;
		}
		return result;
	}

	public AsyncOperationResult updateDataFile(int fileId, Object[] params) {
		try {
			if (!NetworkServices.isNetworkAvailable())
				return AsyncOperationResult.NetworkNotAvailable;
			AsyncOperationResult res = AsyncOperationResult.Success;
			WebQueryService serviceType = WebQueryService.GetDocuments;
			String fileName = AppConfig.Application.getString(fileId);
			switch (fileId) {
			case R.string.fj_lib:
				serviceType = WebQueryService.GetDocuments;
				break;
			case R.string.fj_news:
				serviceType = WebQueryService.GetNews;
				break;
			}
			if (FileServices.checkFileNeedUpdate(AppConfig.DataFullPath
					+ fileName)) {
				String content = new WebQueryServiceProvider()
						.CallWebQueryService(serviceType, params);
				if (content == null)
					res = AsyncOperationResult.ConnectionError;
				else {
					if (fileId == R.string.fj_news
							&& content.indexOf(RSSEndTag) > 0) {
						content = content.substring(
								0,
								content.lastIndexOf(RSSEndTag)
										+ RSSEndTag.length());
						if (!FileServices.updateFileContent(
								AppConfig.DataFullPath, fileName, content,
								"utf-8"))
							res = AsyncOperationResult.Failure;
					} else {
						if (!FileServices.updateFileContent(
								AppConfig.DataFullPath, fileName, content))
							res = AsyncOperationResult.Failure;
					}
				}
			}
			Logger.DoLog("data update status:" + res.name(), "Service:"
					+ serviceType.name(), LogType.Debug);
			return res;

		} catch (Exception e) {
			Logger.DoLog("update data file failed!", e.getMessage(),
					LogType.Error);
			return AsyncOperationResult.Failure;
		}
	}

	public void publishProgressToUI(int progress) {
		this.publishProgress(progress);
	}
}
