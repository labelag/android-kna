/**
 * 
 */
package com.kw.kna.io;

import com.kw.kna.bus.enums.AsyncOperationResult;
import com.kw.kna.bus.enums.AsyncOperationType;

/**
 * @author Mohamad
 * 
 */
public interface OnAsyncOperationListener {

	abstract void onDone(AsyncOperationType type, AsyncOperationResult result,
			Object response);

	abstract void onProgress(AsyncOperationType type, int progress);

}
