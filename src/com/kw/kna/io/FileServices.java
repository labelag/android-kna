package com.kw.kna.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import android.net.Uri;

import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;

public class FileServices {

	public static final String DataFileNameParamSeperator = "-";
	static String _ClassScope = "io.FileServices.";
	static String _ClassCode = "301";

	static String _DataPath = "Data/";

	public static ParsedData readJSONFile(int fileId) {
		return readJSONFile(AppConfig.DataFullPath, fileId);
	}

	public static ParsedData readJSONFile(int fileId, String fileName) {
		return readJSONFile(AppConfig.DataFullPath, fileId, fileName);
	}

	public static ParsedData readJSONFile(String dirPath, int fileId) {
		return readJSONFile(dirPath, fileId, getFileName(fileId));
	}

	public static ParsedData readJSONFile(String dirPath, int fileId,
			String fileName) {
		try {
			// String fileName = getFileName(fileId);
			if (dirPath == null)
				dirPath = AppConfig.DataFullPath;
			return readJSONFile(new FileInputStream(dirPath + fileName), fileId);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ParsedData readJSONFileFromAsset(String path, int fileId) {
		try {
			String fileName = getFileName(fileId);
			return readJSONFile(
					AppConfig.Application.getAssets().open(path + fileName),
					fileId);
		} catch (IOException e) {
			Logger.DoLog("read JSON file from assets failed!", e.getMessage(),
					LogType.Error);
			return null;
		}
	}

	private static ParsedData readJSONFile(InputStream stream, int fileId) {
		String _mCode = _ClassCode + "01";
		String _mScope = _ClassScope + "readJSONFile";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(stream));
			StringBuilder strBuilder = new StringBuilder();
			char[] buff = new char[1024];
			int numRead = 0;
			while ((numRead = reader.read(buff)) != -1) {
				strBuilder.append(String.valueOf(buff, 0, numRead));
			}
			String content = strBuilder.toString();
			if (content == null || content.length() < 1)
				throw new Exception("File content is empty!");

			ParsedData data = JSONParser.parseJSON(content, fileId);
			if (data == null)
				return null;
			return data;// .getObjects();
		} catch (Exception e) {
			Logger.DoLog(_mCode + "99", e.getMessage() + "["
					+ AppConfig.Application.getString(fileId) + "]", _mScope,
					LogType.Error);
			return null;
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	private static String getFileName(int fileId) {
		String fileName = AppConfig.Application.getString(fileId);
		switch (fileId) {
		}
		return fileName;
	}

	public int copyFile(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[128 * 1024];
		int read;
		int total = 0;
		// Logger.DoLog("=========== update file :" + in.available(),
		// LogType.Debug);
		while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
			total += read;
		}
		out.close();
		in.close();
		return total;
	}

	public static String[] getFileNames(int[] fileIds) {

		if (fileIds == null)
			return null;
		String[] fileNames = new String[fileIds.length];
		for (int i = 0; i < fileIds.length; i++) {
			fileNames[i] = getFileName(fileIds[i]);
		}
		return fileNames;
	}

	public static String[] getFileNames(Integer[] fileIds) {

		if (fileIds == null)
			return null;
		String[] fileNames = new String[fileIds.length];
		for (int i = 0; i < fileIds.length; i++) {
			fileNames[i] = getFileName(fileIds[i]);
		}
		return fileNames;
	}

	public static boolean checkFileExist(String filePath) {
		// String dir = AppConfig.InternalDirPath + AppConfig.DataPath;
		File file = new File(filePath);
		return file.exists();
	}

	// public int updateFile(String dirPath, String fileName, InputStream ins) {
	// return updateFile(dirPath + fileName, ins);
	// }
	public int updateFile(String fullPath, InputStream ins) {
		return updateFile(fullPath, ins, 0, null);
	}

	public int updateFile(String fullPath, InputStream ins, int contenLength,
			AsyncOperationTask task) {
		FileOutputStream out = null;
		try {
			File f = new File(fullPath);
			if (!f.exists()) {
				f.getParentFile().mkdirs();
				f.createNewFile();
			}
			byte[] buffer = new byte[128 * 1024];
			int read;
			int total = 0;
			out = new FileOutputStream(fullPath);
			while ((read = ins.read(buffer)) != -1) {
				out.write(buffer, 0, read);
				total += read;
				if (task != null)
					task.publishProgressToUI((total * 100) / contenLength);
			}

			return total;
			// return copyFile(ins, );
		} catch (Exception e) {
			Logger.DoLog("update file in failed! file:" + fullPath,
					e.getMessage(), LogType.Error);
			return -1;
		} finally {
			try {
				if (out != null)
					out.close();
				if (ins != null)
					ins.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static boolean updateFileContent(String dirPath, int fileId,
			String content) {
		return updateFileContent(dirPath,
				AppConfig.Application.getString(fileId), content);
	}

	public static boolean updateFileContent(String dirPath, String fileName,
			String content) {
		return updateFileContent(dirPath, fileName, content, "utf-8");
	}

	public static boolean updateFileContent(String dirPath, String fileName,
			String content, String charset) {
		BufferedWriter writer = null;
		try {
			// content = content.replace("windows-1256", "utf-8");
			File f = new File(dirPath);
			if (!f.exists()) {
				f.mkdirs();
			}
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(dirPath + fileName), charset));
			writer.write(content);
			return true;
		} catch (Exception e) {
			Logger.DoLog("update file content failed!", "file:" + fileName
					+ ";" + e.getMessage(), LogType.Error);
			return false;
		} finally {
			if (writer != null)
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	public int updateFileContent(String dirPath, int fileId, InputStream ins) {
		try {
			String fileName = AppConfig.Application.getString(fileId);
			File f = new File(dirPath);
			if (!f.exists()) {
				f.mkdirs();
			}
			return copyFile(ins, new FileOutputStream(dirPath + fileName));
		} catch (Exception e) {
			Logger.DoLog("update file in " + dirPath + " dir failed! file:"
					+ fileId, e.getMessage(), LogType.Error);
			return -1;
		}
	}

	public static String readInput(InputStream ins) throws IOException {

		BufferedReader buff = new BufferedReader(new InputStreamReader(ins));
		StringBuilder strBuilder = new StringBuilder();
		String read = buff.readLine();
		while (read != null) {
			strBuilder.append(read);
			read = buff.readLine();
		}
		return strBuilder.toString();
	}

	public boolean updateDocImage(String fileLocalPath, String imageUrl) {
		return updateDocImage(fileLocalPath, imageUrl, null);
	}

	public boolean updateDocImage(String fileLocalPath, String imageUrl,
			AsyncOperationTask task) {
		InputStream ins = null;
		try {
			boolean result = false;
			double fileSize = 0;
			int slashIndex = imageUrl.lastIndexOf("/") + 1;
			if (slashIndex > 0) {
				imageUrl = imageUrl.replace(imageUrl.substring(slashIndex),
						Uri.encode(imageUrl.substring(slashIndex)));
			}
			URL url = new URL(imageUrl);
			URLConnection urlcon = url.openConnection();
			// int contentLength;
			Logger.DoLog("File content length : " + urlcon.getContentLength()
					/ 1024 + " KB", imageUrl, LogType.Debug);
			ins = urlcon.getInputStream();
			if (ins == null) {
				result = false;
				Logger.DoLog("retreive image file " + fileLocalPath + " from "
						+ url.getPath() + " failed!", LogType.Warning);
			} else if ((fileSize = updateFile(fileLocalPath, ins,
					urlcon.getContentLength(), task)) < 0) {
				result = false;
				// ins.close();
				Logger.DoLog("update file " + fileLocalPath
						+ " in local failed!", LogType.Warning);
			} else {
				// getCMYKImageFromPath(fileLocalPath);
				result = true;
				// ins.close();
				Logger.DoLog("update image file " + fileLocalPath + " from "
						+ imageUrl + ";  result:" + result, " file size : "
						+ getFileSizeString(fileSize), LogType.Info);
			}
			return result;
		} catch (Exception e) {
			Logger.DoLog("update image file " + fileLocalPath + "; failed! ",
					e.toString(), LogType.Error);
			return false;
		} finally {
			if (ins != null)
				try {
					ins.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	public static String getFileSizeString(double size) {
		if (size < 1024)
			return size + " B";
		return (size / (double) 1024) + " KB";
	}

	public static boolean checkFileNeedUpdate(String filePath) {
		try {
			File f = new File(filePath);
			/* bigger than 5 minutes */
			return (System.currentTimeMillis() - f.lastModified()) > 30000;
		} catch (Exception e) {
		}
		return true;
	}

	public static String createFileNameFromUrl(String url) {
		if (url == null || url.length() == 0)
			return url;
		String name = url.replaceAll("\\W+", "_");
		if (name.length() > 127)
			name = name.substring(name.length() - 256);
		return name;
	}
}
