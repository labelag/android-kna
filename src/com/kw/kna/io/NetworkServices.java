package com.kw.kna.io;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.kw.kna.bus.AppConfig;

public class NetworkServices {

	public static boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) AppConfig.Application
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

}
