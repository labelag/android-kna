package com.kw.kna.io;

public class ParsedData {
	private boolean success;
	private ParsedObject[] objects;

	public ParsedData(boolean success, ParsedObject[] objects) {
		this.success = success;
		this.objects = objects;
	}

	public ParsedObject[] getObjects() {
		return objects;
	}

	public boolean isSuccess() {
		return success;
	}

}
