package com.kw.kna.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import com.kw.kna.bus.AppConfig;
import com.kw.kna.bus.Logger;
import com.kw.kna.bus.Logger.LogType;
import com.kw.kna.bus.enums.WebQueryService;

class WebQueryStringProvider {

	final static String paramSeperator = "&";
	final static String valueIndicator = "=";
}

public class WebQueryServiceProvider {

	public String CallWebQueryService(WebQueryService service, Object[] params) {
		InputStream ins = null;
		try {
			String address = null, queryString = null;
			switch (service) {
			case GetDocuments:
				address = AppConfig.WQS_DataJson;
				queryString = "";
				break;
			case GetNews:
				address = AppConfig.WQS_NewsRSS;
				queryString = "";
				break;
			}

			URL url = new URL(address + queryString);
			ins = url.openConnection().getInputStream();
			return FileServices.readInput(ins);
		} catch (Exception e) {
			Logger.DoLog(
					"Call web query service failed! service:" + service.name(),
					e.getMessage(), LogType.Error);
			return null;
		} finally {
			try {
				if (ins != null)
					ins.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
