package com.kw.kna.io;

public class JSONItemsLexicon {

	public static final String success = "success";
	public static final String data = "data";
	public final static String date = "date";
	public final static String file = "file";
	public final static String title = "title";
	public final static String images = "images";
	public final static String number_of_page = "number_of_page";
	public final static String type = "type";
	public final static String section = "section";
	public final static String year = "year";
	public final static String members = "members";
	public final static String photo = "photo";
	public final static String cv = "cv";
	public final static String name = "name";
	public final static String Horizontalimage = "Horizontalimage";
	public final static String Verticalimage = "Verticalimage";
	public final static String priority = "priority";
	public final static String id = "id";
	public final static String text = "text";
	public final static String map = "map";
	public final static String latitude = "latitude";
	public final static String longtitude = "longtitude";
	public final static String url = "url";

}
