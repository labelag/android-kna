/**
 * 
 */
package com.kw.kna.bus.enums;

import java.util.HashMap;

/**
 * @author Mohamad
 * 
 */
public enum DataType {

	document("document"), gallery_list("gallery-list"), members_list(
			"members-list"), Static("static"), Url("url");

	private final String _type;

	private static final HashMap<String, DataType> typesByValue = new HashMap<String, DataType>();

	static {
		DataType[] vals = DataType.values();
		for (DataType type : vals) {
			typesByValue.put(type._type, type);
		}
	}

	DataType(String type) {
		_type = type;
	}

	public static DataType fromValue(String value) {
		if (typesByValue.containsKey(value))
			return typesByValue.get(value);
		else
			return DataType.document;
	}

	public String getType() {
		return _type;
	}

}
