package com.kw.kna.bus.enums;

import android.util.SparseArray;

public enum WebQueryService {
	GetDocuments(0), GetNews(2);

	private final int _type;
	SparseArray<WebQueryService> rq = new SparseArray<WebQueryService>();

	private static final SparseArray<WebQueryService> typesByValue = new SparseArray<WebQueryService>();

	static {
		WebQueryService[] vals = WebQueryService.values();
		for (WebQueryService type : vals) {
			typesByValue.put(type._type, type);
		}
	}

	WebQueryService(int type) {
		_type = type;
	}

	public int getType() {
		return _type;
	}
}
