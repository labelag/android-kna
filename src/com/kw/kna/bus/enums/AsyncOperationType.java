package com.kw.kna.bus.enums;

import android.util.SparseArray;

public enum AsyncOperationType {
	None(0), UpdateDocuments(1), UpdateFileByUrl(2), UpdateNewsList(3), GetPDF(
			4);

	private final int _type;
	SparseArray<AsyncOperationType> rq = new SparseArray<AsyncOperationType>();

	private static final SparseArray<AsyncOperationType> typesByValue = new SparseArray<AsyncOperationType>();

	static {
		AsyncOperationType[] vals = AsyncOperationType.values();
		for (AsyncOperationType type : vals) {
			typesByValue.put(type._type, type);
		}
	}

	AsyncOperationType(int type) {
		_type = type;
	}

	public static AsyncOperationType fromValue(int value) {
		if (typesByValue.indexOfKey(value) >= 0)
			return typesByValue.get(value);
		else
			return AsyncOperationType.None;
	}

	public int getType() {
		return _type;
	}
}
