package com.kw.kna.bus.enums;

import android.util.SparseArray;

public enum AsyncOperationResult {
	Failure(0), NetworkNotAvailable(1), ConnectionError(2), Success(3), Cancel(
			4);

	private final int _type;
	SparseArray<AsyncOperationResult> rq = new SparseArray<AsyncOperationResult>();

	private static final SparseArray<AsyncOperationResult> typesByValue = new SparseArray<AsyncOperationResult>();

	static {
		AsyncOperationResult[] vals = AsyncOperationResult.values();
		for (AsyncOperationResult type : vals) {
			typesByValue.put(type._type, type);
		}
	}

	AsyncOperationResult(int type) {
		_type = type;
	}

	public static AsyncOperationResult fromValue(int value) {
		if (typesByValue.indexOfKey(value) < 0)
			return typesByValue.get(value);
		else
			return AsyncOperationResult.Failure;
	}

	public int getType() {
		return _type;
	}
}
