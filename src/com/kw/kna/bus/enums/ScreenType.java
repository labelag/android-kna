/**
 * 
 */
package com.kw.kna.bus.enums;

import android.util.SparseArray;

/**
 * @author Mohamad
 * 
 */
public enum ScreenType {

	Phone(0), Tablet(1), TabletLandscape(2);

	private final int _type;

	private static final SparseArray<ScreenType> typesByValue = new SparseArray<ScreenType>();

	static {
		ScreenType[] vals = ScreenType.values();
		for (ScreenType type : vals) {
			typesByValue.put(type._type, type);
		}
	}

	ScreenType(int type) {
		_type = type;
	}

	public static ScreenType fromValue(int value) {
		if (typesByValue.indexOfKey(value) >= 0)
			return typesByValue.get(value);
		else
			return ScreenType.Phone;
	}

	public int getType() {
		return _type;
	}

}
