package com.kw.kna.bus;

import android.util.Log;

public class Logger {
	private static final String tag = "com.kw.kna";

	public enum LogType {
		Error, Info, Warning, Debug

	}

	public static void DoLog(String msg, LogType type) {
		DoLog(msg, null, null, type);
	}

	public static void DoLog(String msg, String trace, LogType type) {
		DoLog(msg, trace, null, type);
	}

	public static void DoLog(String msg, String trace, String scope,
			LogType type) {
		msg = scope + "|" + msg + "|" + trace;
		switch (type) {
		case Debug:
		default:
			Log.d(tag, msg);
			break;
		case Error:
			Log.e(tag, msg);
			break;
		case Warning:
			Log.w(tag, msg);
			break;
		case Info:
			Log.i(tag, msg);
			break;
		}
	}
}
