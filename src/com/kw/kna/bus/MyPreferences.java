/**
 * 
 */
package com.kw.kna.bus;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Mohamad
 * 
 */
public class MyPreferences {

	private final String FirstRun = "pref_FirstRun";

	// private Context mainAct;
	private SharedPreferences settings;

	public MyPreferences() {
		settings = AppConfig.Application.getSharedPreferences(
				AppConfig.PrefrencesName, Context.MODE_PRIVATE);
	}

	public MyPreferences(Context mainAct) {
		settings = mainAct.getSharedPreferences(AppConfig.PrefrencesName,
				Context.MODE_PRIVATE);
	}

	public boolean isFirstRun() {
		return settings.getBoolean(FirstRun, true);
	}

	public void setFirstRun() {
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(FirstRun, false);
		// Commit the edits!
		editor.commit();
	}

}
