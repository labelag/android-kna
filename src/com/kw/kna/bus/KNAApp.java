package com.kw.kna.bus;

import android.app.Application;
import android.content.res.Configuration;

import com.kw.kna.bus.Logger.LogType;
import com.kw.kna.pl.PLConfig;

public class KNAApp extends Application {
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// AppConfig.detectDeviceType(newConfig);
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreate() {
		startupApp(getResources().getConfiguration());
		super.onCreate();
	}

	public void startupApp(Configuration config) {
		Logger.DoLog("Application start up...", LogType.Info);
		AppConfig.Application = this;
		AppConfig.setInternalDirPath(AppConfig.Application.getFilesDir()
				.getPath());
		// AppConfig.detectDeviceType(config);
		PLConfig.init();

		// try {
		// String filePath = AppConfig.DataFullPath
		// + "14/"
		// + URLEncoder.encode("m7ather-almajlis.pdf",
		// PLConfig.Charset_UTF8);
		// int total = new FileServices().copyFile(
		// getAssets().open("m7ather-almajlis.pdf"),
		// new FileOutputStream(filePath));
		// Logger.DoLog((total / 1024) + " KB file copied to " + filePath,
		// LogType.Info);
		// filePath = AppConfig.DataFullPath
		// + "15/"
		// + URLEncoder.encode("m7ather-aldstoor.pdf",
		// PLConfig.Charset_UTF8);
		// total = new FileServices().copyFile(
		// getAssets().open("m7ather-aldstoor.pdf"),
		// new FileOutputStream(filePath));
		// Logger.DoLog((total / 1024) + " KB file copied to " + filePath,
		// LogType.Info);
		// } catch (Exception e) {
		// Logger.DoLog("Test ERRRRRRORRR!", e.toString(), LogType.Error);
		// }
	}
}
