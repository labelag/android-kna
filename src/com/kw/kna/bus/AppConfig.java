package com.kw.kna.bus;

public class AppConfig {
	public static KNAApp Application;
	// public static ScreenType DeviceScreenType;

	public static String InternalDirPath = "/Data/Data/com.kw.kna/files";
	public static String DataFolderUrl = "file:///Data/Data/com.kw.kna/files/data/";
	public static final String DataPath = "Data/";
	public static String DataFullPath = "/Data/Data/com.kw.kna/files/data/";
	public static String ImagesFullPath = "/Data/Data/com.kw.kna/files/images/";
	public static String ImagesFolderUrl = "file:///Data/Data/com.kw.kna/files/images/";

	public static final String WQS_DataJson = "http://www.kna.kw/app/index.json";
	public static final String WQS_NewsRSS = "http://www.kna.kw/clt/rss-mobile-app-request-services-2014.asp";
	// "http://app.labelag.com/rss.php";//
	public static final String PrefrencesName = "kna_pref";
	public static final long SplashDuration = 2000;
	public static final String NewsImagesDir = "news/";
	public static final String CVDirsPrefix = "cv-";

	public static void setInternalDirPath(String path) {
		InternalDirPath = path + "/";
		DataFullPath = InternalDirPath + DataPath;
		DataFolderUrl = "file://" + DataFullPath;
		ImagesFullPath = InternalDirPath + "Images/";
		ImagesFolderUrl = "file://" + ImagesFullPath;
	}

	// public static void detectDeviceType(Configuration newConfig) {
	// boolean IsLandscape = newConfig.orientation ==
	// Configuration.ORIENTATION_LANDSCAPE;
	// int scSize = newConfig.screenLayout
	// & Configuration.SCREENLAYOUT_SIZE_MASK;
	// switch (scSize) {
	// case Configuration.SCREENLAYOUT_SIZE_SMALL:
	// case Configuration.SCREENLAYOUT_SIZE_NORMAL:
	// AppConfig.DeviceScreenType = ScreenType.Phone;
	// break;
	// case Configuration.SCREENLAYOUT_SIZE_LARGE:
	// case Configuration.SCREENLAYOUT_SIZE_XLARGE:
	// if (IsLandscape)
	// AppConfig.DeviceScreenType = ScreenType.TabletLandscape;
	// else
	// AppConfig.DeviceScreenType = ScreenType.Tablet;
	// }
	// }

}
